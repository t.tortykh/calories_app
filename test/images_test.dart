import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:calories/resources/resources.dart';

void main() {
  test('images assets test', () {
    expect(File(Images.addImage).existsSync(), true);
    expect(File(Images.appIcon).existsSync(), true);
    expect(File(Images.calculator).existsSync(), true);
    expect(File(Images.calendar).existsSync(), true);
    expect(File(Images.calories).existsSync(), true);
    expect(File(Images.cancel).existsSync(), true);
    expect(File(Images.check).existsSync(), true);
    expect(File(Images.cross).existsSync(), true);
    expect(File(Images.dish).existsSync(), true);
    expect(File(Images.energyBar).existsSync(), true);
    expect(File(Images.fat).existsSync(), true);
    expect(File(Images.fruitsVegetables).existsSync(), true);
    expect(File(Images.kcal).existsSync(), true);
    expect(File(Images.luchTime).existsSync(), true);
    expect(File(Images.smiling).existsSync(), true);
    expect(File(Images.wallpaper1).existsSync(), true);
    expect(File(Images.wallpaper2).existsSync(), true);
    expect(File(Images.wallpaper3).existsSync(), true);
    expect(File(Images.wallpaper4).existsSync(), true);
    expect(File(Images.wallpaper5).existsSync(), true);
    expect(File(Images.whey).existsSync(), true);
  });
}
