import 'package:calories/global/endpoints/api.dart';
import 'package:calories/global/utils/snack_bar.dart';
import 'package:calories/main.dart';
import 'package:flutter/material.dart';

enum DataState {
  uninitialized,
  refreshing,
  initialFetching,
  moreFetching,
  fetched,
  noMoreData,
  emptyData,
  error
}

class HistoryController extends ChangeNotifier {
  DataState _dataState = DataState.uninitialized;
  bool get _didLastLoad => nextPage == '';
  DataState get dataState => _dataState;
  List<dynamic> _dataList = [];
  List<dynamic> get dataList => _dataList;
  List<dynamic> historyList = [];

  Api api = Api();

  String nextPage = '1';
  checkNextPage(Map<String, dynamic> list) {
    String? nextUrl = list['days']['next_page_url'];

    if (nextUrl != null) {
      var pageArr = nextUrl.split('=');

      this.nextPage = pageArr[1];
    } else {
      return this.nextPage = '';
    }
    print(nextPage);
  }

  fetchData({bool isRefresh = false}) async {
    if (!isRefresh) {
      _dataState = (_dataState == DataState.uninitialized)
          ? DataState.initialFetching
          : DataState.moreFetching;
    } else {
      _dataState = DataState.refreshing;
    }
    notifyListeners();
    try {
      if (_didLastLoad) {
        _dataState = DataState.noMoreData;
      } else {
        List<dynamic> list = await formatingData();

        if (list.isEmpty) {
          _dataState = DataState.emptyData;
        } else {
          if (_dataState == DataState.refreshing) {
            // _dataList.clear();
          }
          _dataList = list;
          _dataState = DataState.fetched;
        }
      }
      notifyListeners();
    } catch (e) {
      _dataState = DataState.error;
      notifyListeners();
      rootScaffoldMessengerKey.currentState?.showSnackBar(
        createSnackBar(showedText: e.toString(), result: false),
      );
    }
  }

  Future formatingData() async {
    Map<String, dynamic> data = await api.getHistory(nextPage);
    checkNextPage(data);

    var path = data['days']['data'];
    if (path.length > 0) {
      for (var i = 0; i < path.length; i++) {
        var day = {'day': path[i]['day']};

        Map<String, List<Map<String, dynamic>>> records = {
          'records': [
            if (path[i]['records'].length > 0)
              for (var j = 0; j < path[i]['records'].length; j++)
                {
                  'name': path[i]['records'][j]['product']['name'],
                  'grams': path[i]['records'][j]['weight'],
                  'kcal': ((path[i]['records'][j]['weight'] *
                          path[i]['records'][j]['product']['calories']) /
                      100),
                  'id': path[i]['records'][j]['id'],
                  'kcal100': path[i]['records'][j]['product']['calories'],
                }
          ]
        };
        var temp = [day, records];
        historyList.add(temp);
      }
      notifyListeners();
      return historyList;
    }
    return [];
  }

  TextEditingController editGrams = TextEditingController();

  Future editRecord(id) async {
    var editedRecord = {
      'grams': editGrams.text,
      'id': id.toString(),
    };
    return await api.editRecord(editedRecord);
  }

  Future deleteRecord(id) async {
    var deletedRecord = {'id': id.toString()};
    return await api.deleteRecord(deletedRecord);
  }
}
