import 'package:calories/lib/global/themes/resources/resources.dart';
import 'package:calories/modules/history/history_controller.dart';
import 'package:calories/modules/history/ui/card_day.dart';
import 'package:calories/modules/new_record/detailed/record_detailed.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HistoryScreen extends StatelessWidget {
  const HistoryScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(title: const Text('History')),
        body: Stack(
          children: [
            Container(
              decoration: const BoxDecoration(
                  image: DecorationImage(
                opacity: 1,
                image: AssetImage(AppImages.wallpaper_5),
                fit: BoxFit.fitHeight,
              )),
            ),
            ChangeNotifierProvider(
              create: (context) => HistoryController(),
              child: Consumer<HistoryController>(builder: (BuildContext context,
                  HistoryController controller, Widget? _) {
                switch (controller.dataState) {
                  case DataState.uninitialized:
                    Future(() {
                      controller.fetchData();
                    });
                    return HistoryListWidget(controller.dataList, true);
                  case DataState.initialFetching:
                    return const Center(child: CircularProgressIndicator());
                  case DataState.moreFetching:
                  case DataState.refreshing:
                    return HistoryListWidget(controller.dataList, true);
                  case DataState.fetched:

                  case DataState.error:
                  case DataState.noMoreData:
                    return HistoryListWidget(controller.dataList, false);
                  case DataState.emptyData:
                    return const Center(
                        child: Text(
                      'You will see your here',
                      style: TextStyle(fontSize: 17),
                    ));
                }
              }),
            ),
          ],
        ));
  }
}
