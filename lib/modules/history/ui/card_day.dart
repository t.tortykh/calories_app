import 'package:calories/global/themes/theme/app_colors.dart';
import 'package:calories/modules/history/history_controller.dart';
import 'package:calories/modules/history/ui/food_card.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class HistoryListWidget extends StatelessWidget {
  final List<dynamic> _data;
  bool _isLoading;
  HistoryListWidget(this._data, this._isLoading, {super.key});
  late DataState _dataState;
  late BuildContext _buildContext;
  late HistoryFoodCard card;

  @override
  Widget build(BuildContext context) {
    _dataState =
        Provider.of<HistoryController>(context, listen: false).dataState;
    _buildContext = context;
    return SafeArea(child: _scrollNotificationWidget());
  }

  Widget _scrollNotificationWidget() {
    return Column(
      children: [
        Expanded(
          child: NotificationListener<ScrollNotification>(
            onNotification: _scrollNotification,
            child: RefreshIndicator(
              onRefresh: () async {
                await _onRefresh();
              },
              child: ListView.builder(
                itemCount: _data.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(10),
                    child: Card(
                      shape: RoundedRectangleBorder(
                          side: const BorderSide(color: Colors.black, width: 2),
                          borderRadius: BorderRadius.circular(10.0)),
                      color: AppColor.yellowLight,
                      elevation: 2,
                      child: Padding(
                        padding:
                            const EdgeInsets.only(bottom: 10, left: 10, top: 2),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(4),
                              child: Text(
                                  DateFormat('d MMMM').format(
                                      DateTime.parse(_data[index][0]['day'])),
                                  style: const TextStyle(fontSize: 23)),
                            ),
                            ListView.builder(
                                physics: const NeverScrollableScrollPhysics(),
                                scrollDirection: Axis.vertical,
                                shrinkWrap: true,
                                itemCount: _data[index][1]['records'].length,
                                itemBuilder: (BuildContext context, int ind) {
                                  final path = _data[index][1]['records'];
                                  return card = HistoryFoodCard(
                                    name: path[ind]['name'],
                                    kcal: path[ind]['kcal'],
                                    grams: path[ind]['grams'].toDouble(),
                                    id: path[ind]['id'],
                                    kcal100: path[ind]['kcal100'].toDouble(),
                                    index: index,
                                  );
                                }),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ),
        if (_dataState == DataState.moreFetching)
          const Center(child: CircularProgressIndicator()),
      ],
    );
  }

  bool _scrollNotification(ScrollNotification scrollInfo) {
    if (!_isLoading &&
        scrollInfo.metrics.pixels == scrollInfo.metrics.maxScrollExtent) {
      _isLoading = true;
      Provider.of<HistoryController>(_buildContext, listen: false).fetchData();
    }
    return true;
  }

  _onRefresh() async {
    if (!_isLoading) {
      _isLoading = true;
      Provider.of<HistoryController>(_buildContext, listen: false)
          .fetchData(isRefresh: true);
    }
  }
}
