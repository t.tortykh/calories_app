import 'package:calories/global/navigator/main_navigation.dart';
import 'package:calories/global/themes/theme/app_colors.dart';
import 'package:calories/global/utils/snack_bar.dart';
import 'package:calories/global/widgets/errors/auth_error.dart';
import 'package:calories/main.dart';
import 'package:calories/modules/history/history_controller.dart';
import 'package:flutter/material.dart';

class HistoryFoodCard extends StatefulWidget {
  final int id;
  final String name;
  double grams;
  double kcal;
  double kcal100;
  int? index;
  bool deleteLine = false;
  HistoryFoodCard({
    super.key,
    required this.id,
    required this.grams,
    required this.kcal,
    required this.name,
    required this.kcal100,
    required this.index,
  });

  @override
  State<HistoryFoodCard> createState() => _HistoryFoodCardState();
}

HistoryController historyController = HistoryController();

class _HistoryFoodCardState extends State<HistoryFoodCard> {
  changeGrams(grams, kcal) {
    setState(() {
      widget.grams = double.parse(historyController.editGrams.text);
      widget.kcal = (widget.grams * widget.kcal100) / 100;
    });
  }

  deleteCard() {
    setState(() {
      widget.deleteLine = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Card(
          elevation: 4.0,
          margin: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 7.0),
          child: Container(
            decoration: const BoxDecoration(
                border: Border(
              left: BorderSide(color: AppColor.coral, width: 5),
            )),
            height: 70,
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      widget.name,
                      style: const TextStyle(
                          fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        'grams',
                        style: TextStyle(fontSize: 12),
                      ),
                      Text(
                        widget.grams.toStringAsFixed(1),
                        style: const TextStyle(fontSize: 12),
                      ),
                    ],
                  ),
                  const SizedBox(width: 10),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        'ccal',
                        style: TextStyle(fontSize: 12),
                      ),
                      Text(
                        widget.kcal.toStringAsFixed(1),
                        style: const TextStyle(fontSize: 12),
                      ),
                    ],
                  ),
                  const SizedBox(width: 10),
                  IconButton(
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (context) {
                            return StatefulBuilder(
                              builder: (context, setState) {
                                historyController.editGrams.text =
                                    widget.grams.toString();
                                return AlertDialog(
                                  title: const Text('Chanhe amount of product'),
                                  content: TextFormField(
                                    controller: historyController.editGrams,
                                  ),
                                  actions: [
                                    TextButton(
                                      onPressed: () => Navigator.pop(context),
                                      child: const Text("Cancel"),
                                    ),
                                    TextButton(
                                      onPressed: () async {
                                        try {
                                          await historyController
                                              .editRecord(widget.id);
                                          changeGrams(
                                              widget.grams, widget.kcal);
                                          Navigator.pop(context);
                                          rootScaffoldMessengerKey.currentState
                                              ?.showSnackBar(createSnackBar(
                                                  showedText: 'Record edited',
                                                  result: true));
                                        } on AuthException catch (e) {
                                          Navigator.of(context)
                                              .pushReplacementNamed(
                                                  MainNavigationRouteNames
                                                      .login);
                                        } catch (e) {
                                          rootScaffoldMessengerKey.currentState
                                              ?.showSnackBar(createSnackBar(
                                                  showedText: e.toString(),
                                                  result: false));
                                        }
                                      },
                                      child: const Text("Change"),
                                    ),
                                  ],
                                );
                              },
                            );
                          },
                        );
                      },
                      icon: const Icon(Icons.edit_outlined)),
                  IconButton(
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (context) {
                            return StatefulBuilder(
                                builder: (context, setState) {
                              return AlertDialog(
                                title: const Text('Delete'),
                                content: const Text(
                                    'Are you shure you want to delete?'),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () =>
                                        Navigator.pop(context, 'Cancel'),
                                    child: const Text('Cancel'),
                                  ),
                                  TextButton(
                                    onPressed: () async {
                                      try {
                                        await historyController
                                            .deleteRecord(widget.id);
                                        deleteCard();
                                        Navigator.pop(context);
                                        rootScaffoldMessengerKey.currentState
                                            ?.showSnackBar(createSnackBar(
                                                showedText: 'Record deleted',
                                                result: true));
                                      } on AuthException catch (e) {
                                        Navigator.of(context)
                                            .pushReplacementNamed(
                                                MainNavigationRouteNames.login);
                                      } catch (e) {
                                        rootScaffoldMessengerKey.currentState
                                            ?.showSnackBar(createSnackBar(
                                                showedText: e.toString(),
                                                result: false));
                                      }
                                    },
                                    child: const Text('Yes'),
                                  ),
                                ],
                              );
                            });
                          },
                        );
                      },
                      icon: const Icon(Icons.delete_outline)),
                ],
              ),
            ),
          ),
        ),
        widget.deleteLine
            ? const Center(
                child: SizedBox(
                  height: 80,
                  child: Divider(
                    thickness: 2,
                    color: Colors.black,
                  ),
                ),
              )
            : const SizedBox()
      ],
    );
  }
}
