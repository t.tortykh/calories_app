import 'dart:io';
import 'package:calories/global/endpoints/api.dart';
import 'package:image_picker/image_picker.dart';

class ProfileController {
  Api api = Api();

  Future<Map<String, dynamic>> getData() async {
    var data = await api.getUserData();
    return data['user'];
  }

  Future<File> getImage() async {
    final ImagePicker _picker = ImagePicker();

    final XFile? image = await _picker.pickImage(source: ImageSource.gallery);

    File file = File(image!.path);
    return file;
  }

  Future postAvatar(File image) async {
    return await api.uploadUserAvatar(image);
  }

  String getGender(gender) {
    if (gender == 'f') {
      gender = 'female';
    } else if (gender == 'm') {
      gender = 'male';
    } else {
      gender = '     ';
    }

    return gender;
  }

  String getHeight(height) {
    if (height != null) {
      height = height.toString();
    } else {
      height = '     ';
    }
    return height;
  }

  String getAge(age) {
    if (age != null) {
      age = age.toString();
    } else {
      age = '     ';
    }
    return age;
  }

  String getWeight(weight) {
    if (weight != null) {
      weight = weight.toString();
    } else {
      weight = '     ';
    }
    return weight;
  }
}
