import 'dart:io';
import 'package:calories/global/navigator/main_navigation.dart';
import 'package:calories/global/themes/theme/app_colors.dart';
import 'package:calories/global/utils/snack_bar.dart';
import 'package:calories/global/widgets/errors/auth_error.dart';
import 'package:calories/lib/global/themes/resources/resources.dart';
import 'package:calories/main.dart';
import 'package:calories/modules/profile/profile_controller.dart';
import 'package:flutter/material.dart';

class AvatarName extends StatefulWidget {
  const AvatarName({
    Key? key,
  }) : super(key: key);

  @override
  State<AvatarName> createState() => _AvatarNameState();
}

ProfileController profileController = ProfileController();

class _AvatarNameState extends State<AvatarName> {
  File imageFile = File('');
  bool isloaded = false;
  Map<String, dynamic>? data;
  String? image;
  bool loadingData = true;
  String? userName;

  pickImage() async {
    File _image = await profileController.getImage();
    if (_image != null) {
      try {
        await profileController.postAvatar(File(_image.path));
        setState(() {
          imageFile = File(_image.path);
          isloaded = true;
        });
      } on AuthException catch (e) {
        Navigator.of(context)
            .pushReplacementNamed(MainNavigationRouteNames.login);
      } catch (e) {
        rootScaffoldMessengerKey.currentState?.showSnackBar(
          createSnackBar(showedText: e.toString(), result: false),
        );
      }
    } else {
      setState(() {
        isloaded = false;
      });
    }
  }

  @override
  void initState() {
    _fetchData();

    super.initState();
  }

  void _fetchData() async {
    try {
      data = await profileController.getData();
      image = data?['image'];
      userName = data?['name'];
    } on AuthException catch (e) {
      Navigator.of(context)
          .pushReplacementNamed(MainNavigationRouteNames.login);
    } catch (e) {
      rootScaffoldMessengerKey.currentState?.showSnackBar(
        createSnackBar(showedText: e.toString(), result: false),
      );
    }
    setState(() {
      loadingData = false;
    });
  }

  Image getImage() {
    if (imageFile.path != '') {
      return Image.file(imageFile);
    } else if (image != null) {
      return Image.network(image!, fit: BoxFit.fill);
    } else {
      return Image.asset(AppImages.smiling);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 130,
          width: 130,
          child: Stack(
            children: [
              loadingData
                  ? const Center(child: CircularProgressIndicator())
                  : (data != null)
                      ? CircleAvatar(
                          radius: 70,
                          backgroundColor: Colors.white,
                          child: Container(
                              margin: const EdgeInsets.all(4.0),
                              child: ClipOval(
                                child: getImage(),
                              )),
                        )
                      : Center(child: Image.asset(AppImages.cancel)),
              Material(
                color: Colors.transparent,
                child: InkWell(
                  borderRadius: const BorderRadius.all(Radius.circular(15)),
                  onTap: () {
                    pickImage();
                  },
                ),
              ),
            ],
          ),
        ),
        loadingData
            ? const Center(child: CircularProgressIndicator())
            : Text(
                userName ?? 'Hi, friend',
                style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 45,
                    color: AppColor.coral),
              ),
        const SizedBox(height: 40),
        loadingData
            ? const SizedBox()
            : profileController.getGender(data?['gender']) != '     '
                ? SizedBox(
                    width: double.infinity,
                    height: 80,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            TextLine(
                              lable: 'Gender:   ',
                              resultLable:
                                  profileController.getGender(data?['gender']),
                            ),
                            TextLine(
                              lable: 'Height:   ',
                              resultLable:
                                  profileController.getHeight(data?['height']),
                            ),
                          ],
                        ),
                        Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              TextLine(
                                lable: 'Age:   ',
                                resultLable:
                                    profileController.getAge(data?['age']),
                              ),
                              TextLine(
                                lable: 'Weight:   ',
                                resultLable: profileController
                                    .getWeight(data?['weight']),
                              ),
                            ])
                      ],
                    ),
                  )
                : const Text(
                    'Fill all required fields in calculator, to see your data',
                    style: TextStyle(fontSize: 15, color: AppColor.tealDark),
                  )
      ],
    );
  }
}

class TextLine extends StatelessWidget {
  final String lable;
  final String resultLable;

  const TextLine({super.key, required this.lable, required this.resultLable});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          lable,
          style: const TextStyle(color: Colors.black, fontSize: 20),
        ),
        Text(
          resultLable,
          style: const TextStyle(
            fontSize: 20,
          ),
        ),
      ],
    );
  }
}
