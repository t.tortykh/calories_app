import 'package:calories/global/providers/session_data_provider/session_data_provider.dart';
import 'package:calories/global/navigator/main_navigation.dart';
import 'package:calories/global/utils/snack_bar.dart';
import 'package:calories/lib/global/themes/resources/resources.dart';
import 'package:calories/main.dart';
import 'package:calories/modules/auth/login/login_widget.dart';
import 'package:calories/modules/profile/ui/avatar_widget.dart';
import 'package:flutter/material.dart';
import 'package:calories/global/utils/styling.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: const Text('Profile'),
      ),
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
                image: DecorationImage(
              opacity: 1,
              image: AssetImage(AppImages.wallpaper_5),
              fit: BoxFit.fitHeight,
            )),
          ),
          SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(
                margin: const EdgeInsets.all(15),
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const AvatarName(),
                    const SizedBox(height: 20),
                    Container(
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      width: double.infinity,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          TextButton(
                              onPressed: () {
                                Navigator.of(context).pushNamed(
                                    MainNavigationRouteNames.daysHistoryList);
                              },
                              child: const Text('My History',
                                  style:
                                      CustomTextStyle.userSettingsTextStyle)),
                          TextButton(
                              onPressed: () {
                                Navigator.of(context).pushNamed(
                                    MainNavigationRouteNames.userCalculation);
                              },
                              child: const Text('Calculate calories',
                                  style:
                                      CustomTextStyle.userSettingsTextStyle)),
                          TextButton(
                            onPressed: () {
                              SessionDataProvider().setToken(null);
                              Navigator.pushAndRemoveUntil(
                                  context,
                                  PageRouteBuilder(
                                    pageBuilder: (BuildContext context,
                                        Animation animation,
                                        Animation secondaryAnimation) {
                                      return const LoginWidget();
                                    },
                                    transitionsBuilder: (BuildContext context,
                                        Animation<double> animation,
                                        Animation<double> secondaryAnimation,
                                        Widget child) {
                                      return SlideTransition(
                                        position: Tween<Offset>(
                                          begin: const Offset(1.0, 0.0),
                                          end: Offset.zero,
                                        ).animate(animation),
                                        child: child,
                                      );
                                    },
                                  ),
                                  (Route route) => false);
                              rootScaffoldMessengerKey.currentState
                                  ?.showSnackBar(createSnackBar(
                                      showedText:
                                          'You just logout! We alrady miss you :)',
                                      result: true));
                            },
                            child: const Text('Logout',
                                style: CustomTextStyle.userSettingsTextStyle),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
