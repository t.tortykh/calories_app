import 'package:calories/global/navigator/main_navigation.dart';
import 'package:calories/global/themes/theme/app_colors.dart';
import 'package:calories/global/utils/snack_bar.dart';
import 'package:calories/global/widgets/errors/auth_error.dart';
import 'package:calories/lib/global/themes/resources/resources.dart';
import 'package:calories/main.dart';
import 'package:calories/modules/product_card/product_card_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ProductCard extends StatefulWidget {
  String name;
  final int id;
  String kcal;
  String proteins;
  String fats;
  String carbs;

  ProductCard(
      {required this.name,
      required this.id,
      required this.kcal,
      required this.proteins,
      required this.fats,
      required this.carbs,
      super.key});

  @override
  State<ProductCard> createState() => _ProductCardState();
}

class _ProductCardState extends State<ProductCard> {
  ProductCardController productCardController = ProductCardController();

  changeUI() {
    setState(() {
      widget.kcal = productCardController.editKcal.text;
      widget.fats = productCardController.editFats.text;
      widget.carbs = productCardController.editCarbs.text;
      widget.proteins = productCardController.editProteins.text;
      widget.name = productCardController.nameProduct.text;
      productCardController.editKcal.clear();
      productCardController.editProteins.clear();
      productCardController.editFats.clear();
      productCardController.editCarbs.clear();
    });
  }

  final GlobalKey<FormState> _kcalKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _proteinsKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _fatsKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _carbsKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _nameProduct = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: FittedBox(
          child: Text(widget.name, maxLines: 3),
        ),
      ),
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
                image: DecorationImage(
              opacity: 1,
              image: AssetImage(AppImages.wallpaper_5),
              fit: BoxFit.fitHeight,
            )),
          ),
          SafeArea(
            child: Column(children: [
              _HeadderKcal(cKal: widget.kcal),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 50, right: 50, top: 30, bottom: 45),
                  child: Stack(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            color: AppColor.tealLight,
                            borderRadius:
                                const BorderRadius.all(Radius.circular(10)),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black.withOpacity(0.2),
                                  blurRadius: 10,
                                  offset: const Offset(2, 2)),
                            ]),
                        width: double.infinity,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            BuilderPFC(
                                mainName: 'Proteins',
                                gramsFromMainMame: widget.proteins),
                            BuilderPFC(
                                mainName: 'Fats',
                                gramsFromMainMame: widget.fats),
                            BuilderPFC(
                                mainName: 'Carbohydrates',
                                gramsFromMainMame: widget.carbs),
                          ],
                        ),
                      ),
                      Positioned(
                        left: 10,
                        bottom: 0,
                        child: Image.asset(
                          AppImages.dish,
                          scale: 2.5,
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ]),
          ),
        ],
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
              heroTag: null,
              elevation: 8,
              backgroundColor: AppColor.yellow,
              shape: const RoundedRectangleBorder(
                  side: BorderSide(width: 3, color: Colors.black),
                  borderRadius: BorderRadius.all(Radius.circular(15.0))),
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (context) {
                      return StatefulBuilder(
                        builder: (context, setState) {
                          productCardController.editKcal.text = widget.kcal;
                          productCardController.editProteins.text =
                              widget.proteins;
                          productCardController.editFats.text = widget.fats;
                          productCardController.editCarbs.text = widget.carbs;
                          productCardController.nameProduct.text = widget.name;

                          return AlertDialog(
                            title: const Text(
                              'Chanhe product',
                              style: TextStyle(color: AppColor.tealDark),
                            ),
                            content: SingleChildScrollView(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  _EditedNameTextField(
                                    nameProduct: _nameProduct,
                                    productCardController:
                                        productCardController,
                                  ),
                                  const _NameField(name: 'Product name'),
                                  _EditedNumTextField(
                                    controller: productCardController.editKcal,
                                    keyForm: _kcalKey,
                                    inputAction: TextInputAction.next,
                                  ),
                                  const _NameField(name: 'kcal'),
                                  _EditedNumTextField(
                                    controller:
                                        productCardController.editProteins,
                                    keyForm: _proteinsKey,
                                    inputAction: TextInputAction.next,
                                  ),
                                  const _NameField(name: 'Proteins'),
                                  _EditedNumTextField(
                                    controller: productCardController.editFats,
                                    keyForm: _fatsKey,
                                    inputAction: TextInputAction.next,
                                  ),
                                  const _NameField(name: 'Fats'),
                                  _EditedNumTextField(
                                    controller: productCardController.editCarbs,
                                    keyForm: _carbsKey,
                                    inputAction: TextInputAction.done,
                                  ),
                                  const _NameField(name: 'Carbohydrates'),
                                ],
                              ),
                            ),
                            actions: [
                              TextButton(
                                onPressed: () => Navigator.pop(context),
                                child: const Text("Cancel"),
                              ),
                              TextButton(
                                onPressed: () async {
                                  try {
                                    var body = {
                                      'id': widget.id.toString(),
                                      'name': productCardController
                                          .nameProduct.text,
                                      'calories':
                                          productCardController.editKcal.text,
                                      'proteins': productCardController
                                          .editProteins.text,
                                      'fats':
                                          productCardController.editFats.text,
                                      'carbohydrates':
                                          productCardController.editCarbs.text,
                                    };

                                    await productCardController
                                        .editProduct(body);

                                    rootScaffoldMessengerKey.currentState
                                        ?.showSnackBar(createSnackBar(
                                            showedText: 'Changes are seved',
                                            result: true));
                                    changeUI();
                                    Navigator.of(context).popAndPushNamed(
                                        MainNavigationRouteNames
                                            .categoriesScreen);
                                    rootScaffoldMessengerKey.currentState
                                        ?.showSnackBar(createSnackBar(
                                            showedText: 'Record edited',
                                            result: true));
                                  } on AuthException catch (e) {
                                    Navigator.of(context).pushReplacementNamed(
                                        MainNavigationRouteNames.login);
                                  } catch (e) {
                                    Navigator.of(context).popAndPushNamed(
                                        MainNavigationRouteNames
                                            .categoriesScreen);
                                    rootScaffoldMessengerKey.currentState
                                        ?.showSnackBar(createSnackBar(
                                            showedText: e.toString(),
                                            result: false));
                                  }
                                },
                                child: const Text("Change"),
                              ),
                            ],
                          );
                        },
                      );
                    });
              },
              child: const Icon(Icons.edit_outlined, color: Colors.black)),
          const SizedBox(height: 25),
          FloatingActionButton(
            elevation: 8,
            backgroundColor: AppColor.coral,
            shape: const RoundedRectangleBorder(
                side: BorderSide(width: 3, color: Colors.black),
                borderRadius: BorderRadius.all(Radius.circular(15.0))),
            onPressed: () {
              showDialog(
                context: context,
                builder: (context) {
                  return StatefulBuilder(builder: (context, setState) {
                    return AlertDialog(
                      title: const Text('Delete'),
                      content: const Text('Are you shure you want to delete?'),
                      actions: <Widget>[
                        TextButton(
                          onPressed: () => Navigator.pop(context, 'Cancel'),
                          child: const Text('Cancel'),
                        ),
                        TextButton(
                          onPressed: () async {
                            try {
                              await productCardController
                                  .deleteProduct(widget.id);
                              Navigator.of(context).popAndPushNamed(
                                  MainNavigationRouteNames.categoriesScreen);
                              rootScaffoldMessengerKey.currentState
                                  ?.showSnackBar(createSnackBar(
                                      showedText: 'Product has been deleted',
                                      result: true));
                            } on AuthException catch (e) {
                              Navigator.of(context).pushReplacementNamed(
                                  MainNavigationRouteNames.login);
                            } catch (e) {
                              Navigator.of(context).popAndPushNamed(
                                  MainNavigationRouteNames.categoriesScreen);
                              rootScaffoldMessengerKey.currentState
                                  ?.showSnackBar(createSnackBar(
                                      showedText: e.toString(), result: false));
                            }
                          },
                          child: const Text('Yes'),
                        ),
                      ],
                    );
                  });
                },
              );
            },
            heroTag: null,
            child:
                const Icon(Icons.delete_outline_outlined, color: Colors.black),
          ),
        ],
      ),
    );
  }
}

class _EditedNameTextField extends StatelessWidget {
  const _EditedNameTextField({
    Key? key,
    required GlobalKey<FormState> nameProduct,
    required this.productCardController,
  })  : _nameProduct = nameProduct,
        super(key: key);

  final GlobalKey<FormState> _nameProduct;
  final ProductCardController productCardController;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _nameProduct,
      child: TextFormField(
        controller: productCardController.nameProduct,
        keyboardType: TextInputType.datetime,
        textInputAction: TextInputAction.next,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        validator: (value) {
          if (value == null || value == '') {
            return 'Fill product name';
          } else {
            return null;
          }
        },
      ),
    );
  }
}

class _NameField extends StatelessWidget {
  final String name;
  const _NameField({
    Key? key,
    required this.name,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      name,
      style: const TextStyle(color: AppColor.coral),
    );
  }
}

class _EditedNumTextField extends StatelessWidget {
  const _EditedNumTextField({
    Key? key,
    required GlobalKey<FormState> keyForm,
    required this.controller,
    required this.inputAction,
  }) : super(key: key);

  final TextEditingController controller;
  final TextInputAction inputAction;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: key,
      child: TextFormField(
        controller: controller,
        autocorrect: false,
        keyboardType: TextInputType.datetime,
        inputFormatters: [
          FilteringTextInputFormatter.allow((RegExp("[.0-9]")))
        ],
        textInputAction: inputAction,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        validator: ((value) {
          if (value == null || value == '') {
            return 'Fill the amount';
          }
          if (!RegExp(r'^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$')
              .hasMatch(value)) {
            return 'Please, provide the correct amount';
          } else {
            return null;
          }
        }),
      ),
    );
  }
}

class _HeadderKcal extends StatelessWidget {
  final String cKal;
  const _HeadderKcal({
    Key? key,
    required this.cKal,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      child: Center(
        child: RichText(
          text: TextSpan(
            children: [
              TextSpan(
                text: cKal.toString(),
                style: const TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 35,
                ),
              ),
              const TextSpan(
                text: '  kcal',
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 25,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class BuilderPFC extends StatelessWidget {
  final String mainName;
  final String gramsFromMainMame;
  const BuilderPFC({
    Key? key,
    required this.mainName,
    required this.gramsFromMainMame,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(mainName),
        RichText(
          text: TextSpan(
            children: [
              TextSpan(
                text: gramsFromMainMame.toString(),
                style: const TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 40,
                ),
              ),
              const TextSpan(
                text: 'g',
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 23,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
