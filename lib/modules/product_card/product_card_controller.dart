import 'package:calories/global/endpoints/api.dart';
import 'package:flutter/material.dart';

class ProductCardController {
  TextEditingController editKcal = TextEditingController();

  TextEditingController editProteins = TextEditingController();
  TextEditingController editFats = TextEditingController();
  TextEditingController editCarbs = TextEditingController();
  TextEditingController nameProduct = TextEditingController();

  Api api = Api();

  Future deleteProduct(id) async {
    var deletedProduct = {'id': id.toString()};
    return await api.deleteProduct(deletedProduct);
  }

  Future editProduct(body) async {
    return await api.editProduct(body);
  }
}
