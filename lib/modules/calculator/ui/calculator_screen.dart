import 'package:calories/global/navigator/main_navigation.dart';
import 'package:calories/global/themes/theme/app_colors.dart';
import 'package:calories/global/utils/snack_bar.dart';
import 'package:calories/global/widgets/errors/auth_error.dart';
import 'package:calories/lib/global/themes/resources/resources.dart';
import 'package:calories/modules/calculator/calculator_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:core';

import '../../../main.dart';

class CalculationScreen extends StatefulWidget {
  const CalculationScreen({super.key});

  @override
  State<CalculationScreen> createState() => _CalculationScreenState();
}

class _CalculationScreenState extends State<CalculationScreen> {
  bool _isLoading = true;
  List<Map<String, String>> goalList = [];

  CalculatorController calcController = CalculatorController();
  @override
  void initState() {
    super.initState();
    loadScreen();
  }

  Future<void> loadScreen() async {
    try {
      await calcController.initUser();
    } on AuthException catch (e) {
      Navigator.of(context)
          .pushReplacementNamed(MainNavigationRouteNames.login);
    } catch (e) {
      rootScaffoldMessengerKey.currentState?.showSnackBar(
          createSnackBar(showedText: e.toString(), result: false));
    }

    setState(() {
      if (calcController.user['gender'] != null) {
        goalList = calcController.selectGoal(calcController.user['gender']);
      }
      _isLoading = false;
    });
  }

  final GlobalKey<FormState> _formKeyName = GlobalKey<FormState>();
  final GlobalKey<FormState> _formKeyAge = GlobalKey<FormState>();
  final GlobalKey<FormState> _formKeyWeight = GlobalKey<FormState>();
  final GlobalKey<FormState> _formKeyHeight = GlobalKey<FormState>();
  final GlobalKey<FormState> _formKeyGoal = GlobalKey<FormState>();
  final GlobalKey<FormState> _formKeyActivity = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: const Text('Calories Calculator'),
      ),
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
                image: DecorationImage(
              opacity: 1,
              image: AssetImage(AppImages.wallpaper_5),
              fit: BoxFit.fitHeight,
            )),
          ),
          _isLoading
              ? const Center(child: CircularProgressIndicator())
              : SafeArea(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(30.0),
                      child: Column(
                        children: [
                          _NameTextField(
                              formKeyName: _formKeyName,
                              calcController: calcController),
                          const SizedBox(height: 10),
                          _AgeTextField(
                              formKeyAge: _formKeyAge,
                              calcController: calcController),
                          _buildGenderRadioList(),
                          const SizedBox(height: 10),
                          _WeightTextField(
                              formKeyWeight: _formKeyWeight,
                              calcController: calcController),
                          const SizedBox(height: 25),
                          _HightTextField(
                              formKeyHeight: _formKeyHeight,
                              calcController: calcController),
                          const SizedBox(height: 25),
                          _buildGoalDropDown(_formKeyGoal),
                          const SizedBox(height: 25),
                          _buildActivityDropDown(_formKeyActivity),
                          const SizedBox(height: 25),
                          PostCalculate(
                            formKeyName: _formKeyName,
                            formKeyAge: _formKeyAge,
                            formKeyWeight: _formKeyWeight,
                            formKeyHeight: _formKeyHeight,
                            fromKeyGoal: _formKeyGoal,
                            fromKeyActivity: _formKeyActivity,
                            calcController: calcController,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
        ],
      ),
    );
  }

  FormField<bool> _buildGenderRadioList() {
    return FormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      builder: (FormFieldState<bool> state) {
        return Column(
          children: [
            RadioListTile(
              activeColor: AppColor.tealDark,
              title: const Text('Female'),
              value: 'f',
              groupValue: calcController.gender,
              onChanged: (value) {
                setState(() {
                  calcController.user['ratio_id'] = null;
                  calcController.choosenGoalId = null;
                  calcController.gender = value.toString();
                  goalList = calcController.selectGoal(calcController.gender);
                });
              },
            ),
            RadioListTile(
              activeColor: AppColor.tealDark,
              title: const Text('Male'),
              value: 'm',
              groupValue: calcController.gender,
              onChanged: (value) {
                setState(() {
                  calcController.user['ratio_id'] = null;
                  calcController.choosenGoalId = null;
                  calcController.gender = value.toString();
                  goalList = calcController.selectGoal(calcController.gender);
                });
              },
            ),
          ],
        );
      },
      validator: (value) {
        if (value != true) {
          return 'Please choose gender';
        }
        return null;
      },
    );
  }

  Form _buildActivityDropDown(GlobalKey<FormState> _formKeyActivity) {
    return Form(
      key: _formKeyActivity,
      child: DropdownButtonFormField(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        validator: (value) => value == null ? 'Field required' : null,
        decoration: const InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: AppColor.yellow, width: 3),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: AppColor.yellow, width: 3),
          ),
        ),
        itemHeight: null,
        isExpanded: true,
        disabledHint: const Text('Choose Activity'),
        hint: const Text('Choose Activity'),
        items: calcController.dropActivity.map((item) {
          return DropdownMenuItem(
            value: calcController.dropActivity.indexOf(item),
            child: Text(item['activity'].toString()),
          );
        }).toList(),
        onChanged: (newVal) {
          setState(() {
            calcController.choosenActivityId = newVal;
          });
        },
        value: calcController.choosenActivityId,
      ),
    );
  }

  Form _buildGoalDropDown(GlobalKey<FormState> _formKeyGoal) {
    return Form(
      key: _formKeyGoal,
      child: DropdownButtonFormField(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        validator: (value) => value == null ? 'Field required' : null,
        decoration: const InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: AppColor.yellow, width: 3),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: AppColor.yellow, width: 3),
          ),
        ),
        itemHeight: null,
        isExpanded: true,
        hint: const Text('Choose your type or your goal'),
        items: goalList.map((item) {
          return DropdownMenuItem(
            value: goalList.indexOf(item),
            child: Text(item['goal'].toString()),
          );
        }).toList(),
        onChanged: (newVal) {
          setState(() {
            calcController.choosenGoalId = newVal;
          });
        },
        value: calcController.choosenGoalId,
      ),
    );
  }
}

class _NameTextField extends StatelessWidget {
  const _NameTextField({
    Key? key,
    required GlobalKey<FormState> formKeyName,
    required this.calcController,
  })  : _formKeyName = formKeyName,
        super(key: key);

  final GlobalKey<FormState> _formKeyName;
  final CalculatorController calcController;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKeyName,
      child: TextFormField(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        controller: calcController.userName,
        textInputAction: TextInputAction.next,
        decoration: _decorationTextField('Your Name'),
        validator: (value) {
          if (value == null || value == '') {
            return 'Fill your name';
          } else {
            return null;
          }
        },
      ),
    );
  }
}

class _AgeTextField extends StatelessWidget {
  const _AgeTextField({
    Key? key,
    required GlobalKey<FormState> formKeyAge,
    required this.calcController,
  })  : _formKeyAge = formKeyAge,
        super(key: key);

  final GlobalKey<FormState> _formKeyAge;
  final CalculatorController calcController;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKeyAge,
      child: TextFormField(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        keyboardType: TextInputType.datetime,
        inputFormatters: [FilteringTextInputFormatter.allow((RegExp("[0-9]")))],
        controller: calcController.age,
        textInputAction: TextInputAction.next,
        decoration: _decorationTextField('Your age'),
        validator: ((value) {
          if (value == null || value == '') {
            return 'Fill your age';
          } else {
            return null;
          }
        }),
      ),
    );
  }
}

class _WeightTextField extends StatelessWidget {
  const _WeightTextField({
    Key? key,
    required GlobalKey<FormState> formKeyWeight,
    required this.calcController,
  })  : _formKeyWeight = formKeyWeight,
        super(key: key);

  final GlobalKey<FormState> _formKeyWeight;
  final CalculatorController calcController;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKeyWeight,
      child: TextFormField(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        keyboardType: TextInputType.datetime,
        inputFormatters: [
          FilteringTextInputFormatter.allow((RegExp("[.0-9]")))
        ],
        controller: calcController.weight,
        textInputAction: TextInputAction.next,
        decoration: _decorationTextField('Your weight'),
        validator: ((value) {
          if (value == null || value == '') {
            return 'Fill your weigth';
          }
          if (!RegExp(r'^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$')
              .hasMatch(value)) {
            return 'Please, provide the correct data';
          } else {
            return null;
          }
        }),
      ),
    );
  }
}

class _HightTextField extends StatelessWidget {
  const _HightTextField({
    Key? key,
    required GlobalKey<FormState> formKeyHeight,
    required this.calcController,
  })  : _formKeyHeight = formKeyHeight,
        super(key: key);

  final GlobalKey<FormState> _formKeyHeight;
  final CalculatorController calcController;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKeyHeight,
      child: TextFormField(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        keyboardType: TextInputType.datetime,
        inputFormatters: [
          FilteringTextInputFormatter.allow((RegExp("[.0-9]")))
        ],
        controller: calcController.height,
        textInputAction: TextInputAction.next,
        decoration: _decorationTextField('Your height'),
        validator: ((value) {
          if (value == null || value == '') {
            return 'Fill your height';
          }
          if (!RegExp(r'^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$')
              .hasMatch(value)) {
            return 'Please, provide the correct data';
          } else {
            return null;
          }
        }),
      ),
    );
  }
}

class PostCalculate extends StatelessWidget {
  final formKeyName;
  final formKeyAge;
  final formKeyWeight;
  final formKeyHeight;
  final fromKeyGoal;
  final fromKeyActivity;
  final CalculatorController calcController;

  const PostCalculate({
    Key? key,
    required this.formKeyName,
    required this.formKeyAge,
    required this.formKeyWeight,
    required this.formKeyHeight,
    required this.fromKeyGoal,
    required this.fromKeyActivity,
    required this.calcController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () async {
        formKeyName.currentState.validate();
        formKeyAge.currentState.validate();
        formKeyWeight.currentState.validate();
        formKeyHeight.currentState.validate();
        fromKeyGoal.currentState.validate();
        fromKeyActivity.currentState.validate();

        if (formKeyName.currentState.validate() &&
            formKeyAge.currentState.validate() &&
            formKeyWeight.currentState.validate() &&
            formKeyHeight.currentState.validate() &&
            fromKeyGoal.currentState.validate() &&
            fromKeyActivity.currentState.validate()) {
          try {
            await calcController.calculations();

            Navigator.pushNamedAndRemoveUntil(context,
                MainNavigationRouteNames.mainScreen, ModalRoute.withName(''));

            rootScaffoldMessengerKey.currentState?.showSnackBar(createSnackBar(
                showedText: 'We saved your changes', result: true));
          } on AuthException catch (e) {
            Navigator.of(context)
                .pushReplacementNamed(MainNavigationRouteNames.login);
          } catch (e) {
            rootScaffoldMessengerKey.currentState?.showSnackBar(
                createSnackBar(showedText: e.toString(), result: false));
          }
        }
      },
      style: ButtonStyle(
          padding: MaterialStateProperty.all<EdgeInsets>(
              const EdgeInsets.symmetric(vertical: 10, horizontal: 30)),
          backgroundColor: MaterialStateProperty.all(Colors.amber),
          shape: MaterialStateProperty.all(RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
          ))),
      child: const Text(
        'Calculate',
        style: TextStyle(fontSize: 33),
      ),
    );
  }
}

InputDecoration _decorationTextField(String lableName) {
  return InputDecoration(
    label: Text(lableName),
    contentPadding: const EdgeInsets.all(10),
    border: const OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(20.0)),
    ),
  );
}
