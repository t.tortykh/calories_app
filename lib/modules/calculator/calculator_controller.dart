import 'package:calories/global/endpoints/api.dart';
import 'package:flutter/material.dart';

class CalculatorController {
  var user;
  Api api = Api();
  final userName = TextEditingController();
  final age = TextEditingController();
  String? gender;
  final weight = TextEditingController();
  final height = TextEditingController();
  var choosenGoalId;
  var choosenActivityId;

  Map<String, dynamic> userData = {};

  initUser() async {
    user = await api.getUserData();
    user = user['user'];
    userName.text = user['name'] != null ? user['name'].toString() : '';
    gender = user['gender'] != null ? user['gender'].toString() : '';
    age.text = user['age'] != null ? user['age'].toString() : '';
    weight.text = user['weight'] != null ? user['weight'].toString() : '';
    height.text = user['height'] != null ? user['height'].toString() : '';
    choosenGoalId = user['ratio_id'] ?? 0;
    choosenActivityId = user['activity_id'] ?? 0;
    return user;
  }

  calculations() async {
    userData['name'] = userName.text;
    userData['age'] = age.text;
    userData['gender'] = gender;
    userData['weight'] = weight.text;
    userData['height'] = height.text;
    userData['ratio_id'] =
        choosenGoalId != null ? (choosenGoalId).toString() : '';
    userData['goal'] = gender == 'f'
        ? goalFemale[int.parse(userData['ratio_id'])]['ratio']
        : goalMale[int.parse(userData['ratio_id'])]['ratio'];
    userData['activity_id'] =
        choosenActivityId != null ? (choosenActivityId).toString() : '';
    userData['activity'] =
        dropActivity[int.parse(userData['activity_id'])]['index'].toString();
    await api.calculateCalories(userData);
  }

  static const List<Map<String, String>> goalFemale = [
    {'goal': 'Balanced type of metabolism', 'ratio': '4-9-4'},
    {'goal': 'Protein type of metabolism', 'ratio': '5-3-2'},
    {'goal': 'Carbohydrate type of metabolism', 'ratio': '25-15-60'},
    {'goal': 'Weight loss', 'ratio': '4-2-4'},
    {'goal': 'Gain muscle', 'ratio': '4.7-2.3-3'},
    {'goal': 'Weight loss and gain muscle', 'ratio': '5-2-3'},
    {'goal': 'Normal weight gain', 'ratio': '3-2-5'},
  ];
  static const List<Map<String, String>> goalMale = [
    {'goal': 'Non-physical job', 'ratio': '3.3-2.5-4.2'},
    {'goal': 'Physical job', 'ratio': '2.7-2.3-5'},
    {'goal': 'Weight loss and gain muscle', 'ratio': '3-1.5-5.5'},
  ];

  Map<String, List<Map<String, String>>> goals = {
    'f': goalFemale,
    'm': goalMale,
  };

  selectGoal(gender) {
    return goals[gender];
  }

  List<Map<String, Object>> dropActivity = [
    {
      'activity': 'Basal Metabolic Rate',
      'index': 1,
    },
    {
      'activity': 'Sedentary: little or no exercise',
      'index': 1.2,
    },
    {
      'activity': 'Light: exercise 1-3 times/week',
      'index': 1.375,
    },
    {
      'activity': 'Moderate: exercise 4-5 times/week',
      'index': 1.465,
    },
    {
      'activity': 'Active: daily exercise or intense exercise 3-4 times/week',
      'index': 1.55
    },
    {
      'activity': 'Very Active: intense exercise 6-7 times/week',
      'index': 1.725
    },
    {
      'activity': 'Extra Active: very intense exercise daily, or physical job ',
      'index': 1.9
    }
  ];

  final nameTextController = TextEditingController();

  Future calculate(Map<String, dynamic> userData) async {
    return api.calculateCalories(userData);
  }
}
