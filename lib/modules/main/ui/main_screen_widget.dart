import 'package:calories/global/navigator/main_navigation.dart';
import 'package:calories/global/utils/snack_bar.dart';
import 'package:calories/global/widgets/errors/auth_error.dart';
import 'package:calories/lib/global/themes/resources/resources.dart';
import 'package:calories/main.dart';
import 'package:calories/modules/main/main_screen_controller.dart';
import 'package:calories/global/themes/theme/app_colors.dart';
import 'package:calories/modules/main/ui/three_floating_buttons.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:calories/modules/main/ui/info_boxes_column.dart';

class MainScreenWidget extends StatefulWidget {
  const MainScreenWidget({super.key});

  @override
  State<MainScreenWidget> createState() => _MainScreenWidgetState();
}

class _MainScreenWidgetState extends State<MainScreenWidget> {
  Map<String, dynamic>? dayStat;
  bool _isLoading = true;
  Map<String, dynamic>? userData;
  String name = 'friend';
  int? maxKcal;
  int? maxCarbs;
  int? maxProt;
  int? maxFats;

  MainScreenController controller = MainScreenController();

  @override
  initState() {
    getAllData();
    super.initState();
  }

  getAllData() {
    getData().then((response) => fetchData()).then((value) => setState(
          () {
            _isLoading = false;
          },
        ));
  }

  Future fetchData() async {
    try {
      var response = await controller.getStatistic();
      if (!response['data'].isEmpty) {
        dayStat = response['data'][0];
      } else {
        dayStat = null;
      }
      return dayStat;
    } on AuthException catch (e) {
      Navigator.of(context)
          .pushReplacementNamed(MainNavigationRouteNames.login);
    } catch (e) {
      rootScaffoldMessengerKey.currentState?.showSnackBar(
        createSnackBar(showedText: e.toString(), result: false),
      );
    }
  }

  Future getData() async {
    try {
      var response = await controller.getUserData();
      userData = response['user'];
      name = userData?['name'];
      maxKcal = userData?['calories'];
      maxCarbs = userData?['carbs'];
      maxProt = userData?['proteins'];
      maxFats = userData?['fats'];
    } on AuthException catch (e) {
      Navigator.of(context)
          .pushReplacementNamed(MainNavigationRouteNames.login);
    } catch (e) {
      rootScaffoldMessengerKey.currentState?.showSnackBar(
        createSnackBar(showedText: e.toString(), result: false),
      );
    }
    return userData;
  }

  Future _refresh() {
    return getData().then((response) => fetchData()).then((value) => setState(
          () {
            _isLoading = false;
          },
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: _isLoading
            ? const SizedBox()
            : Text(
                'Hi,$name!',
                style: const TextStyle(
                    color: AppColor.coral,
                    fontFamily: 'Rajdhani',
                    fontSize: 40),
              ),
        actions: [
          IconButton(
              onPressed: (() {
                setState(() {
                  _isLoading = true;
                  _refresh();
                });
              }),
              icon: const Icon(Icons.rotate_left_sharp))
        ],
      ),
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
                image: DecorationImage(
              opacity: 1,
              image: AssetImage(AppImages.wallpaper_4),
              fit: BoxFit.fitHeight,
            )),
          ),
          _isLoading
              ? const Center(child: CircularProgressIndicator())
              : SafeArea(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text(
                          DateFormat('d MMMM').format(DateTime.now()),
                          style: const TextStyle(
                              fontSize: 17, fontWeight: FontWeight.w400),
                        ),
                        const SizedBox(height: 30),
                        Image.asset(AppImages.kcal),
                        const SizedBox(height: 20),
                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: dayStat == null ||
                                        dayStat?['day'] !=
                                            DateFormat('y-MM-dd')
                                                .format(DateTime.now())
                                    ? '0'
                                    : dayStat?['calories'].toStringAsFixed(1),
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 55,
                                    color: Colors.indigo,
                                    fontFamily: 'Rajdhani'),
                              ),
                              maxKcal == null
                                  ? const TextSpan(
                                      text: 'kcal',
                                      style: TextStyle(
                                          fontStyle: FontStyle.italic,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                          color: Colors.black,
                                          fontFamily: 'Rajdhani'),
                                    )
                                  : TextSpan(
                                      text: '/ ${maxKcal.toString()} kcal',
                                      style: const TextStyle(
                                          fontStyle: FontStyle.italic,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                          color: Colors.black,
                                          fontFamily: 'Rajdhani'),
                                    ),
                            ],
                          ),
                        ),
                        // const _CircleChart(),
                        const SizedBox(height: 30),
                        InfoBoxesColumn(
                            dayStat: dayStat,
                            maxCarbs: maxCarbs,
                            maxProt: maxProt,
                            maxFats: maxFats)
                      ],
                    ),
                  ),
                ),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton:
          _isLoading ? const SizedBox() : const ThreeFloatingButtons(),
    );
  }
}
