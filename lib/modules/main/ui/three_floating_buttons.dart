import 'package:calories/global/navigator/main_navigation.dart';
import 'package:flutter/material.dart';

import 'package:calories/global/themes/theme/app_colors.dart';

class ThreeFloatingButtons extends StatelessWidget {
  const ThreeFloatingButtons({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          SizedBox(
            child: FloatingActionButton(
              backgroundColor: AppColor.blue,
              shape: const RoundedRectangleBorder(
                  side: BorderSide(width: 3, color: Colors.black),
                  borderRadius: BorderRadius.all(Radius.circular(15.0))),
              onPressed: () {
                // Navigator.pushNamedAndRemoveUntil(
                //     context,
                //     MainNavigationRouteNames.weekStats,
                //     ModalRoute.withName(''));
                Navigator.of(context)
                    .pushNamed(MainNavigationRouteNames.weekStats);
              },
              heroTag: null,
              child: const Icon(Icons.bar_chart, color: Colors.black),
            ),
          ),
          SizedBox(
            child: FloatingActionButton(
              backgroundColor: AppColor.blue,
              shape: const RoundedRectangleBorder(
                side: BorderSide(width: 3, color: Colors.black),
                borderRadius: BorderRadius.all(Radius.circular(15)),
              ),
              onPressed: () {
                Navigator.of(context)
                    .pushNamed(MainNavigationRouteNames.createRecord);
              },
              heroTag: null,
              child: const Icon(Icons.add, color: Colors.black),
            ),
          ),
          SizedBox(
            child: FloatingActionButton(
              backgroundColor: AppColor.blue,
              shape: const RoundedRectangleBorder(
                side: BorderSide(width: 3, color: Colors.black),
                borderRadius: BorderRadius.all(Radius.circular(15)),
              ),
              onPressed: () {
                Navigator.of(context)
                    .pushNamed(MainNavigationRouteNames.userInfo);
              },
              heroTag: null,
              child: const Icon(Icons.perm_identity_sharp, color: Colors.black),
            ),
          )
        ],
      ),
    );
  }
}
