import 'package:calories/global/navigator/main_navigation.dart';
import 'package:calories/global/themes/theme/app_colors.dart';
import 'package:calories/lib/global/themes/resources/resources.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class InfoBoxesColumn extends StatelessWidget {
  static const double highOfBox = 120;
  static const double widthOfBox = double.infinity;
  Map<String, dynamic>? dayStat;

  int? maxCarbs;
  int? maxProt;
  int? maxFats;

  InfoBoxesColumn(
      {required this.dayStat,
      required this.maxCarbs,
      required this.maxProt,
      required this.maxFats,
      super.key});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SingleChildScrollView(
        child: Column(children: [
          _buildingStatsContaainer(context),
          const SizedBox(height: 30),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _buildingCalcContainer(context),
              const SizedBox(width: 20),
              _buildingFoodButton(context),
            ],
          ),
        ]),
      ),
    );
  }

  Expanded _buildingFoodButton(BuildContext context) {
    return Expanded(
      child: Container(
        clipBehavior: Clip.hardEdge,
        height: highOfBox,
        decoration: BoxDecoration(
            color: AppColor.yellow,
            borderRadius: const BorderRadius.all(Radius.circular(15)),
            boxShadow: [
              BoxShadow(
                  color: Colors.black.withOpacity(0.2),
                  blurRadius: 10,
                  offset: const Offset(2, 2)),
            ]),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              side: const BorderSide(
                width: 4,
                color: Colors.black,
              ),
              backgroundColor: AppColor.coral),
          onPressed: () {
            Navigator.of(context)
                .pushNamed(MainNavigationRouteNames.categoriesScreen)
                .whenComplete;
          },
          child: const Text('Food', style: TextStyle(fontSize: 30)),
        ),
      ),
    );
  }

  Expanded _buildingCalcContainer(context) {
    return Expanded(
      child: Container(
        height: highOfBox,
        decoration: infoBoxDecoration(),
        child: Stack(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(AppImages.calculator, height: 65),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Text(
                      'Calorie',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    Text('calculator',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ],
                )
              ],
            ),
            Material(
              color: Colors.transparent,
              child: InkWell(
                borderRadius: const BorderRadius.all(Radius.circular(15)),
                onTap: () {
                  Navigator.of(context)
                      .pushNamed(MainNavigationRouteNames.userCalculation);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Container _buildingStatsContaainer(context) {
    return Container(
      height: highOfBox,
      width: widthOfBox,
      decoration: infoBoxDecoration(),
      child: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Row(
                    children: [
                      SizedBox(
                        height: 45,
                        child: Image.asset(AppImages.energyBar),
                      ),
                      Column(
                        children: [
                          Text(
                            dayStat == null ||
                                    dayStat?['day'] !=
                                        DateFormat('y-MM-dd')
                                            .format(DateTime.now())
                                ? '0'
                                : dayStat?['carbs'].toStringAsFixed(1),
                            style: const TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                          ),
                          maxCarbs == null
                              ? const Text('carbs',
                                  style: TextStyle(
                                    fontSize: 17,
                                    color: Colors.black,
                                  ))
                              : Column(
                                  children: [
                                    Text('/ ${maxCarbs.toString()}',
                                        style: const TextStyle(
                                          fontSize: 12,
                                          color: Colors.black,
                                        )),
                                    const Text('carbs',
                                        style: TextStyle(
                                          fontSize: 17,
                                          color: Colors.black,
                                        )),
                                  ],
                                )
                        ],
                      )
                    ],
                  ),
                  Row(
                    children: [
                      SizedBox(
                        height: 45,
                        child: Image.asset(AppImages.whey),
                      ),
                      Column(
                        children: [
                          Text(
                            dayStat == null ||
                                    dayStat?['day'] !=
                                        DateFormat('y-MM-dd')
                                            .format(DateTime.now())
                                ? '0'
                                : dayStat?['proteins'].toStringAsFixed(1),
                            style: const TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                          ),
                          maxProt == null
                              ? const Text('proteins',
                                  style: TextStyle(
                                    fontSize: 17,
                                    color: Colors.black,
                                  ))
                              : Column(
                                  children: [
                                    Text('/ ${maxProt.toString()}',
                                        style: const TextStyle(
                                          fontSize: 12,
                                          color: Colors.black,
                                        )),
                                    const Text('proteins',
                                        style: TextStyle(
                                          fontSize: 17,
                                          color: Colors.black,
                                        )),
                                  ],
                                )
                        ],
                      )
                    ],
                  ),
                  Row(
                    children: [
                      SizedBox(
                        height: 45,
                        child: Image.asset(AppImages.fat),
                      ),
                      Column(
                        children: [
                          Text(
                            dayStat == null ||
                                    dayStat?['day'] !=
                                        DateFormat('y-MM-dd')
                                            .format(DateTime.now())
                                ? '0'
                                : dayStat?['fats'].toStringAsFixed(1),
                            style: const TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                          ),
                          maxFats == null
                              ? const Text('fats',
                                  style: TextStyle(
                                    fontSize: 17,
                                    color: Colors.black,
                                  ))
                              : Column(
                                  children: [
                                    Text('/ ${maxFats.toString()}',
                                        style: const TextStyle(
                                          fontSize: 12,
                                          color: Colors.black,
                                        )),
                                    const Text('fats',
                                        style: TextStyle(
                                          fontSize: 17,
                                          color: Colors.black,
                                        )),
                                  ],
                                )
                        ],
                      )
                    ],
                  ),
                ],
              ),
            ],
          ),
          Material(
            color: Colors.transparent,
            child: InkWell(
              borderRadius: const BorderRadius.all(Radius.circular(15)),
              onTap: () {
                Navigator.of(context)
                    .pushNamed(MainNavigationRouteNames.daysHistoryList);
              },
            ),
          ),
        ],
      ),
    );
  }

  BoxDecoration infoBoxDecoration() {
    return BoxDecoration(
        border: Border.all(color: Colors.black, width: 4),
        color: AppColor.yellow,
        borderRadius: const BorderRadius.all(Radius.circular(15)),
        boxShadow: [
          BoxShadow(
              color: Colors.black.withOpacity(0.2),
              blurRadius: 10,
              offset: const Offset(2, 2)),
        ]);
  }
}
