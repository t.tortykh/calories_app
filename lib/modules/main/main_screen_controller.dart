import 'package:calories/global/endpoints/api.dart';

class MainScreenController {
  Api api = Api();

  Future getUserData() async {
    return await api.getUserData();
  }

  Future getStatistic() async {
    return await api.getStatistics('2');
  }
}
