// import 'package:calories/global/navigator/main_navigation.dart';
import 'package:calories/lib/global/themes/resources/resources.dart';
import 'package:calories/modules/statistics/ui/bar_stat_wigdet.dart';
import 'package:flutter/material.dart';

class WeekStatisticScreen extends StatelessWidget {
  const WeekStatisticScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Week stats'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          SizedBox(
            height: 130,
            child: Image.asset(
              AppImages.luchTime,
            ),
          ),
          const Expanded(child: BarStatWidget()),
        ],
      ),
    );
  }
}
