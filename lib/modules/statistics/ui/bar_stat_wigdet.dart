// import 'package:calories/global/themes/resources/resources.dart';
import 'package:calories/global/navigator/main_navigation.dart';
import 'package:calories/global/utils/snack_bar.dart';
import 'package:calories/global/widgets/errors/auth_error.dart';
import 'package:calories/lib/global/themes/resources/resources.dart';
import 'package:calories/modules/statistics/week_statistic_controller.dart';
import 'package:calories/global/themes/theme/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:sliver_bar_chart/sliver_bar_chart.dart';

import '../../../main.dart';

class BarStatWidget extends StatefulWidget {
  const BarStatWidget({Key? key}) : super(key: key);

  @override
  BarStatWidgetState createState() => BarStatWidgetState();
}

class BarStatWidgetState extends State<BarStatWidget>
    with TickerProviderStateMixin {
  final ScrollController _scrollController = ScrollController();
  late final List<BarChartData> _barValues;

  final double _minHeight = AppBar().preferredSize.height;
  final double _xAxisTextRotationAngle = 180.0;
  final int _yAxisIntervalCount = 8;
  double _maxHeight = 500.0;
  double _maxWidth = 10.0;

  final bool _restrain = false;
  final bool _fluctuating = false;
  bool _isScrolling = true;

  bool _isLoading = true;
  List<BarChartData> dayStat = [];
  List<dynamic> dayStatsCard = [];

  WeekStatisticController controller = WeekStatisticController();
  Future<void> fetchData() async {
    try {
      var responseDayStat = await controller.prepareData();
      dayStat = controller.dataForChart(responseDayStat);
      dayStatsCard = controller.dataForCards(responseDayStat);
    } on AuthException catch (e) {
      Navigator.of(context)
          .pushReplacementNamed(MainNavigationRouteNames.login);
    } catch (e) {
      rootScaffoldMessengerKey.currentState?.showSnackBar(
        createSnackBar(showedText: e.toString(), result: false),
      );
    }
  }

  @override
  void initState() {
    fetchData().then((value) => {
          setState(
            () {
              _isLoading = false;
            },
          )
        });

    _scrollController.addListener(() {
      setState(() {
        if (_scrollController.offset.roundToDouble() < 100.0) {
          _maxHeight = 500.0;
          _maxWidth = 10.0;
          _isScrolling = true;
        } else {
          if (_scrollController.offset.roundToDouble() >= 400.0) {
            _maxWidth = _scrollController.offset;
          }
          _isScrolling = false;
        }
      });
    });
    super.initState();
  }

  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return _isLoading
        ? const Center(child: CircularProgressIndicator())
        : Scaffold(
            body: SafeArea(
              child: (dayStat.isNotEmpty)
                  ? CustomScrollView(
                      controller: _scrollController,
                      slivers: [
                        SliverBarChart(
                          restrain: _restrain,
                          fluctuating: _fluctuating,
                          minHeight: _minHeight,
                          maxHeight: _maxHeight,
                          maxWidth: _maxWidth,
                          barWidget: BarChartWidget(
                            maxHeight: _maxHeight,
                            minHeight: _minHeight,
                            barValues: dayStat,
                            isScrolling: _isScrolling,
                            yAxisIntervalCount: _yAxisIntervalCount,
                            xAxisTextRotationAngle: _xAxisTextRotationAngle,
                          ),
                        ),
                        SliverList(
                          delegate: SliverChildBuilderDelegate(
                            (BuildContext context, int index) =>
                                _buildingCardStat(index),
                            childCount: dayStatsCard.length,
                          ),
                        ),
                      ],
                    )
                  : Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text(
                            'You will see your statistics here',
                            style: TextStyle(fontSize: 25),
                          ),
                          Image.asset(AppImages.calories)
                        ],
                      ),
                    ),
            ),
            resizeToAvoidBottomInset: false,
          );
  }

  Padding _buildingCardStat(int index) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 7),
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(color: Colors.black, width: 4),
            color: AppColor.blue,
            borderRadius: const BorderRadius.all(Radius.circular(15)),
            boxShadow: [
              BoxShadow(
                  color: Colors.black.withOpacity(0.2),
                  blurRadius: 10,
                  offset: const Offset(2, 2)),
            ]),
        height: 90,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                SizedBox(
                  height: 40,
                  child: Image.asset(AppImages.calendar),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      dayStatsCard[index].date,
                      style: const TextStyle(
                        fontSize: 23,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    ),
                    Text(
                      dayStatsCard[index].month,
                      style: const TextStyle(
                        fontSize: 15,
                        color: Colors.black,
                      ),
                    ),
                  ],
                )
              ],
            ),
            Row(
              children: [
                SizedBox(
                  height: 40,
                  child: Image.asset(AppImages.energyBar),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      dayStatsCard[index].carbs.toString(),
                      style: const TextStyle(
                        fontSize: 23,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    ),
                    const Text('carbs',
                        style: TextStyle(
                          fontSize: 15,
                          color: Colors.black,
                        )),
                  ],
                )
              ],
            ),
            Row(
              children: [
                SizedBox(
                  height: 40,
                  child: Image.asset(AppImages.whey),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      dayStatsCard[index].proteins.toString(),
                      style: const TextStyle(
                        fontSize: 23,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    ),
                    const Text(
                      'proteins',
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.black,
                      ),
                    ),
                  ],
                )
              ],
            ),
            Row(
              children: [
                SizedBox(
                  height: 40,
                  child: Image.asset(AppImages.fat),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      dayStatsCard[index].fats.toString(),
                      style: const TextStyle(
                        fontSize: 23,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    ),
                    const Text(
                      'fats',
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.black,
                      ),
                    ),
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
