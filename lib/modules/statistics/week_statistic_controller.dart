import 'package:calories/global/endpoints/api.dart';
import 'package:calories/global/models/week_stat_card_model.dart';
import 'package:calories/global/themes/theme/app_colors.dart';
import 'package:sliver_bar_chart/sliver_bar_chart.dart';

class WeekStatisticController {
  Api api = Api();

  prepareData() async {
    var responseData = await api.getStatistics("7");
    return responseData['data'] as List<dynamic>;
  }

  List<BarChartData> dataForChart(data) {
    List<BarChartData> dayStat = [];

    for (var i = data.length - 1; i >= 0; i--) {
      var temp = BarChartData(
        x: formatDateMonth(data[i]['day']),
        y: data[i]['calories'].toDouble(),
        barColor: AppColor.coral,
      );
      dayStat.add(temp);
    }

    return dayStat;
  }

  dataForCards(data) {
    var dayStatForCard = [];

    for (var i = 0; i <= data.length - 1; i++) {
      var temp = WeekStatCardModel(
        date: formatData(data[i]['day']),
        month: getMonthFromDate(data[i]['day']),
        carbs: data[i]['carbs'].toInt(),
        proteins: data[i]['proteins'].toInt(),
        fats: data[i]['fats'].toInt(),
      );

      dayStatForCard.add(temp);
    }

    return dayStatForCard;
  }

  formatDateMonth(date) {
    var newDayMonth = '';
    var dateArr = date.split('-');
    if (dateArr.length > 2) {
      newDayMonth = dateArr[2] + '/' + dateArr[1];
    }
    return newDayMonth;
  }

  formatData(date) {
    var newDate = '';
    var dateArr = date.split('-');
    if (dateArr.length > 2) {
      newDate = dateArr[2];
    }
    return newDate;
  }

  getMonthFromDate(date) {
    var month = '';
    var months = [
      '',
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'June',
      'July',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec'
    ];
    var dateArr = date.split('-');
    if (dateArr.length > 2) {
      var num = int.parse(dateArr[1]);
      month = months[num];
    }
    return month;
  }
}
