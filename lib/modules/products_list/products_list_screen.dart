import 'package:calories/global/models/food_list_products/food_list_products.dart';
import 'package:calories/global/utils/snack_bar.dart';
import 'package:calories/global/widgets/errors/auth_error.dart';
import 'package:calories/lib/global/themes/resources/resources.dart';
import 'package:calories/main.dart';
import 'package:calories/modules/product_card/product_card.dart';
import 'package:calories/modules/products_list/products_controller.dart';
import 'package:calories/global/navigator/main_navigation.dart';
import 'package:calories/global/themes/theme/app_colors.dart';
import 'package:flutter/material.dart';

class ProductsListScreen extends StatefulWidget {
  final String catId;
  final String catName;
  const ProductsListScreen(
      {super.key, required this.catId, required this.catName});
  @override
  State<ProductsListScreen> createState() => _ProductsListScreenState();
}

class _ProductsListScreenState extends State<ProductsListScreen> {
  bool _isLoading = true;
  String? nextPage = '1';

  late List<FoodListProducts> productList = [];

  bool loadCompleted = false;

  ProductsController productController = ProductsController();

  Future<void> fetchData() async {
    if (nextPage != '') {
      try {
        var productsResponse =
            await productController.getData(nextPage, widget.catId);

        var nextPageUrl = productsResponse['products']['next_page_url'];

        if (nextPageUrl != null) {
          var pageArr = nextPageUrl.split('=');
          nextPage = pageArr[1];
        } else {
          nextPage = '';
          loadCompleted = true;
        }

        productList = productList
          ..addAll(productController.prepareData(productsResponse));
      } on AuthException catch (e) {
        Navigator.of(context)
            .pushReplacementNamed(MainNavigationRouteNames.login);
      } catch (e) {
        rootScaffoldMessengerKey.currentState?.showSnackBar(
          createSnackBar(showedText: e.toString(), result: false),
        );
      }
      setState(() {
        _isLoading = false;
      });
    }
  }

  final _searchController = TextEditingController();

  void _searchFood() {}

  ScrollController _scrollController = ScrollController();

  void scrollindecator() {
    _scrollController.addListener(
      () {
        if (_scrollController.offset >=
                _scrollController.position.maxScrollExtent &&
            !_scrollController.position.outOfRange) {
          // print('reach to bottom botton');
          if (!loadCompleted) {
            setState(() {
              fetchData();
              //add more data to list
            });
          }
        }
      },
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  initState() {
    super.initState();
    fetchData();
    scrollindecator();

    _searchController.addListener(_searchFood);
  }

  _onProductTap(foodProduct) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ProductCard(
                name: foodProduct.foodName,
                id: foodProduct.id,
                kcal: foodProduct.kcalProduct,
                proteins: foodProduct.proteinsProduct,
                fats: foodProduct.fatsProduct,
                carbs: foodProduct.carbsProduct)));
    // Navigator.of(context)
    // Navigator.pushNamed(
    //   context,
    //   MainNavigationRouteNames.productScreen,
    //   arguments: FoodListProducts(
    //     id: foodProduct.id,
    //     foodName: foodProduct.foodName,
    //     kcalProduct: foodProduct.kcalProduct,
    //     carbsProduct: foodProduct.carbsProduct,
    //     fatsProduct: foodProduct.fatsProduct,
    //     proteinsProduct: foodProduct.proteinsProduct,
    //   ),
    // );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: Text('Category ${widget.catName}'),
      ),
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
                image: DecorationImage(
              opacity: 1,
              image: AssetImage(AppImages.wallpaper_3),
              fit: BoxFit.fitHeight,
            )),
          ),
          _isLoading
              ? const Center(child: CircularProgressIndicator())
              : (productList.isNotEmpty)
                  ? SafeArea(
                      child: _buildingListOfFoodItems(),
                    )
                  : Center(child: Image.asset(AppImages.cancel)),
        ],
      ),
    );
  }

  ListView _buildingListOfFoodItems() {
    return ListView.builder(
      physics: const ScrollPhysics(),
      shrinkWrap: true,
      itemCount: productList.length,
      controller: _scrollController,
      keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
      itemExtent: 110,
      itemBuilder: (BuildContext context, int index) {
        final foodProduct = productList[index];
        if (index == productList.length - 1 && !loadCompleted) {
          return const Center(
            child: Opacity(
              opacity: 1.0,
              child: CircularProgressIndicator(),
            ),
          );
        } else {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10),
            child: Stack(
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: AppColor.lightest,
                      borderRadius: const BorderRadius.only(
                          bottomRight: Radius.circular(20),
                          topRight: Radius.circular(20)),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.3),
                            blurRadius: 8,
                            offset: const Offset(2, 2)),
                      ]),
                  child: Container(
                    decoration: const BoxDecoration(
                      border: Border(
                        left: BorderSide(color: AppColor.coral, width: 5),
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                foodProduct.foodName,
                                style: const TextStyle(
                                    fontSize: 23, fontWeight: FontWeight.bold),
                              ),
                              const Text(
                                'in 100 gramm',
                                style: TextStyle(fontSize: 11),
                              ),
                              Row(
                                children: [
                                  const Text('kcal: '),
                                  Text(
                                    foodProduct.kcalProduct == 'null'
                                        ? '0'
                                        : foodProduct.kcalProduct,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  const Text('fats: '),
                                  Text(
                                    foodProduct.fatsProduct == 'null'
                                        ? '0'
                                        : foodProduct.fatsProduct,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  const Text('carbs: '),
                                  Text(
                                    foodProduct.carbsProduct == 'null'
                                        ? '0'
                                        : foodProduct.carbsProduct,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  const Text('proteins: '),
                                  Text(
                                    foodProduct.proteinsProduct == 'null'
                                        ? '0'
                                        : foodProduct.proteinsProduct,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Material(
                  color: Colors.transparent,
                  child: InkWell(
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                    onTap: () => _onProductTap(foodProduct),
                  ),
                ),
              ],
            ),
          );
        }
      },
    );
  }
}
