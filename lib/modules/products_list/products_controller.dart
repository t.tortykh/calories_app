import 'package:calories/global/endpoints/api.dart';
import 'package:calories/global/models/food_list_products/food_list_products.dart';

class ProductsController {
  Api api = Api();

  getData(page, catId) async {
    return await api.getProducts(page, catId);
  }

  List<FoodListProducts> prepareData(data) {
    List<FoodListProducts> foodList = [];
    var listData = data['products']['data'];

    if (listData != null) {
      for (var i = 0; i <= listData.length - 1; i++) {
        var temp = FoodListProducts(
          id: listData[i]['id'],
          foodName: listData[i]['name'],
          carbsProduct: listData[i]['carbohydrates'] != null
              ? listData[i]['carbohydrates'].toStringAsFixed(1)
              : '0',
          fatsProduct: listData[i]['fats'] != null
              ? listData[i]['fats'].toStringAsFixed(1)
              : '0',
          kcalProduct: listData[i]['calories'] != null
              ? listData[i]['calories'].toStringAsFixed(1)
              : '0',
          proteinsProduct: listData[i]['proteins'] != null
              ? listData[i]['proteins'].toStringAsFixed(1)
              : '0',
        );
        foodList.add(temp);
      }
    }

    return foodList;
  }
}
