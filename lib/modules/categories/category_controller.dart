import 'package:calories/global/endpoints/api.dart';

class CategoryController {
  Api api = Api();

  prepareData() async {
    try {
      var categoryResponse = await api.getCategories();

      var listOfCategory = categoryResponse['categories'] as List<dynamic>;

      List<Map<String, dynamic>> categories = [];
      for (var i = 0; i < listOfCategory.length; i++) {
        Map<String, dynamic> item = {};
        item['id'] = listOfCategory[i]['id'];
        item['name'] = listOfCategory[i]['name'];
        item['image'] = listOfCategory[i]['img'];
        item['description'] = listOfCategory[i]['description'];
        categories.add(item);
      }
      return categories;
    } catch (e) {
      rethrow;
    }
  }

  // getProductList(query) async {
  //   var productList = await api.searchProduct(query);
  //   return productList;
  // }
}
