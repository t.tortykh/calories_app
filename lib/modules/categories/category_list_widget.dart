import 'package:calories/global/navigator/main_navigation.dart';
import 'package:calories/global/utils/snack_bar.dart';
import 'package:calories/global/widgets/errors/auth_error.dart';
import 'package:calories/lib/global/themes/resources/resources.dart';
import 'package:calories/main.dart';
import 'package:calories/modules/categories/category_controller.dart';
import 'package:calories/modules/categories/category_card.dart';
import 'package:flutter/material.dart';

class CategoryListWidget extends StatelessWidget {
  const CategoryListWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(title: const Text('Choose category')),
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
                image: DecorationImage(
              opacity: 1,
              image: AssetImage(AppImages.wallpaper_5),
              fit: BoxFit.fitHeight,
            )),
          ),
          const _ListOfCategoriesBuilder(),
        ],
      ),
    );
  }
}

class _ListOfCategoriesBuilder extends StatefulWidget {
  const _ListOfCategoriesBuilder({
    Key? key,
  }) : super(key: key);

  @override
  State<_ListOfCategoriesBuilder> createState() =>
      _ListOfCategoriesBuilderState();
}

class _ListOfCategoriesBuilderState extends State<_ListOfCategoriesBuilder> {
  List<Map<String, dynamic>>? _categories;
  bool _isLoading = true;
  List<dynamic> productList = [];

  CategoryController categoryController = CategoryController();

  @override
  void initState() {
    super.initState();
    fetchCategory();
  }

  Future<void> fetchCategory() async {
    try {
      _categories = await categoryController.prepareData();
    } on AuthException catch (e) {
      Navigator.of(context)
          .pushReplacementNamed(MainNavigationRouteNames.login);
    } catch (e) {
      rootScaffoldMessengerKey.currentState?.showSnackBar(
          createSnackBar(showedText: e.toString(), result: false));
    }

    setState(() {
      _isLoading = false;
    });
  }

  TextEditingController _searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return _isLoading
        ? const Center(child: CircularProgressIndicator())
        : (_categories != null)
            ? SafeArea(
                child: Stack(
                  children: [
                    ListView.builder(
                      physics: const ScrollPhysics(),
                      shrinkWrap: true,
                      // padding: const EdgeInsets.only(top: 80),
                      itemCount: _categories?.length,
                      itemExtent: 140,
                      itemBuilder: (BuildContext context, int index) {
                        return CategoryCard(
                          name: _categories?[index]['name'],
                          image: _categories?[index]['image'],
                          description: _categories?[index]['description'],
                          id: _categories?[index]['id'],
                        );
                      },
                    ),
                    // Padding(
                    //   padding: const EdgeInsets.all(25.0),
                    //   child: TextField(
                    //     controller: _searchController,
                    //     decoration: InputDecoration(
                    //       labelText: 'Search product ',
                    //       filled: true,
                    //       fillColor: Colors.white.withAlpha(235),
                    //       border: const OutlineInputBorder(),
                    //     ),
                    //   ),
                    // ),
                  ],
                ),
              )
            : Center(child: Image.asset(AppImages.cancel));
  }
}
