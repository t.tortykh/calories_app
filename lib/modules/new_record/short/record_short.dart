import 'package:calories/global/utils/snack_bar.dart';
import 'package:calories/global/widgets/errors/auth_error.dart';
import 'package:calories/global/widgets/popup_window/popup_window.dart';
import 'package:calories/lib/global/themes/resources/resources.dart';
import 'package:calories/modules/new_record/record_controller.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';

import 'package:calories/global/themes/theme/app_colors.dart';
import 'package:calories/global/navigator/main_navigation.dart';

import '../../../main.dart';
import 'search_provider.dart';

final _nameProductController = TextEditingController();
var selectedId = '';
var finalDate;
double containerHeight = 0;
String _responseMessage = '';
bool _isLoading = false;

class RecordShort extends StatefulWidget {
  const RecordShort({super.key});

  @override
  State<RecordShort> createState() => _RecordShortState();
}

class _RecordShortState extends State<RecordShort> {
  final SearchedProvider _provider = SearchedProvider();
  String _responseMessage = '';
  bool _isLoading = false;
  updateHeight(double value) {
    setState(() {
      containerHeight = value;
    });
  }

  @override
  void initState() {
    _provider.setAction(updateHeight);
    super.initState();
  }

  DateTime _selectedDate = DateTime.now();

  void _presentDatePicker() {
    showDatePicker(
            context: context,
            initialDate: DateTime.now(),
            firstDate: DateTime(2023),
            lastDate: DateTime.now())
        .then((pickedDate) {
      if (pickedDate == null) {
        return;
      }
      setState(() {
        _selectedDate = pickedDate;
      });
    });
  }

  final controller = RecordController();
  closePopUpWindow() {
    setState(() {
      if (_responseMessage == 'Success') {
        _nameProductController.clear();
        controller.gramsTextController.clear();
      }
      _responseMessage = '';
    });
  }

  final GlobalKey<FormState> _formKeySearchProd = GlobalKey<FormState>();
  final GlobalKey<FormState> _formKeyGrams = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    finalDate = DateFormat('y-MM-dd').format(_selectedDate);
    final _showingDate = DateFormat('d MMMM').format(_selectedDate);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(title: const Text('New Record')),
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
                image: DecorationImage(
              opacity: 1,
              image: AssetImage(AppImages.wallpaper_5),
              fit: BoxFit.fitHeight,
            )),
          ),
          ChangeNotifierProvider<SearchedProvider>(
            create: (_) => _provider,
            child: SafeArea(
              child: SingleChildScrollView(
                child: Stack(
                  clipBehavior: Clip.none,
                  children: [
                    Column(
                      children: [
                        const SizedBox(height: 40),
                        _buildingSearchTextField(_formKeySearchProd),
                        _buildingGramsTextField(_formKeyGrams),
                        buildingDatePicker(finalDate, _showingDate),
                        postButton(_formKeySearchProd, _formKeyGrams),
                        TextButton(
                            onPressed: () {
                              Navigator.of(context).pushNamed(
                                  MainNavigationRouteNames.newProduct);
                            },
                            child: const Text(
                              'Add New Product',
                              style: TextStyle(
                                color: AppColor.coral,
                                fontSize: 18,
                                decoration: TextDecoration.underline,
                              ),
                            ))
                      ],
                    ),
                    customDropDownMenu(),
                    _responseMessage != ''
                        ? PopUpWindow(
                            responseMessage: _responseMessage,
                            isLoading: _isLoading,
                            action: closePopUpWindow,
                          )
                        : const SizedBox(),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Positioned customDropDownMenu() {
    return Positioned(
      left: 15,
      right: 15,
      top: 65 + 40,
      child: Container(
        decoration: _dropDownDecoration(),
        height: containerHeight,
        child: Consumer<SearchedProvider>(builder: (context, provider, w) {
          return Column(
            children: [
              Expanded(
                  child: SearchList(
                provider: provider,
              )),
              const Divider(
                thickness: 2,
              ),
              TextButton.icon(
                  // style: ButtonStyle(),
                  onPressed: () {
                    Navigator.of(context)
                        .pushNamed(MainNavigationRouteNames.newProduct);
                  },
                  icon: const Icon(
                    Icons.add_circle_outline_outlined,
                    color: AppColor.coral,
                  ),
                  label: const Text(
                    'Add New Product',
                    style: TextStyle(fontSize: 24, color: AppColor.coral),
                  )),
            ],
          );
        }),
      ),
    );
  }

  Padding postButton(_formKeySearchProd, _formKeyGrams) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 100),
      child: ElevatedButton(
        style: _buttonStyle(),
        onPressed: () async {
          _formKeySearchProd.currentState.validate();
          _formKeyGrams.currentState.validate();
          if (_formKeySearchProd.currentState.validate() &&
              _formKeyGrams.currentState.validate()) {
            setState(() {
              _isLoading = true;
            });

            try {
              if (selectedId == '') {
                throw Exception('Product not found. Use Add New Product');
              }
              var response =
                  await controller.createShortRecord(selectedId, finalDate);
              _responseMessage = response['message'] ?? 'Success';
            } on AuthException catch (e) {
              Navigator.of(context)
                  .pushReplacementNamed(MainNavigationRouteNames.login);
            } catch (e) {
              rootScaffoldMessengerKey.currentState?.showSnackBar(
                createSnackBar(showedText: e.toString(), result: false),
              );
            }

            setState(() {
              _responseMessage = _responseMessage;
              _isLoading = false;
            });
          }
        },
        child: const Text(
          'Add Record',
          style: TextStyle(fontSize: 33, color: Colors.black),
        ),
      ),
    );
  }

  Padding buildingDatePicker(String finalDate, _showingDate) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
              _selectedDate == null
                  ? 'No date choosen!'
                  : 'Date: $_showingDate',
              style: const TextStyle(color: AppColor.tealDark)),
          TextButton(
            onPressed: _presentDatePicker,
            child: const Text(
              'Change Date',
              style: TextStyle(color: AppColor.coral),
            ),
          ),
        ],
      ),
    );
  }

  Padding _buildingGramsTextField(_formKeyGrams) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 20),
      child: Form(
        key: _formKeyGrams,
        child: TextFormField(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          controller: controller.gramsTextController,
          keyboardType: TextInputType.datetime,
          inputFormatters: [
            FilteringTextInputFormatter.allow((RegExp("[.0-9]")))
          ],
          decoration: _textFieldDecoration(
              labelText: 'Grams', icon: const Icon(Icons.scale_outlined)),
          validator: (value) {
            if (value == null || value == '') {
              return 'Fill the amount of grams';
            }
            if (!RegExp(r'^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$')
                .hasMatch(value)) {
              return 'Please, provide the correct data';
            } else {
              return null;
            }
          },
        ),
      ),
    );
  }

  Padding _buildingSearchTextField(_formKeySearchProd) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      child: Form(
        key: _formKeySearchProd,
        child: TextFormField(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          controller: _nameProductController,
          onChanged: (val) {
            if (val.isNotEmpty) {
              _provider.callAction(350.toDouble());
              _provider.updateProductList(val);
            }
          },
          decoration: _textFieldDecoration(
              labelText: 'Find your product', icon: const Icon(Icons.search)),
          validator: (value) {
            if (value == null || value == '') {
              return 'Find the product';
            } else {
              return null;
            }
          },
        ),
      ),
    );
  }

  ButtonStyle _buttonStyle() {
    return ButtonStyle(
        padding: MaterialStateProperty.all<EdgeInsets>(
            const EdgeInsets.symmetric(vertical: 10, horizontal: 30)),
        backgroundColor: MaterialStateProperty.all(AppColor.green),
        shape: MaterialStateProperty.all(RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
        )));
  }

  BoxDecoration _dropDownDecoration() {
    return BoxDecoration(
        color: AppColor.blue,
        border: Border.all(color: Colors.black, width: 4),
        borderRadius: const BorderRadius.all(Radius.circular(15)),
        boxShadow: [
          BoxShadow(
              color: Colors.black.withOpacity(0.2),
              blurRadius: 10,
              offset: const Offset(2, 2)),
        ]);
  }

  InputDecoration _textFieldDecoration(
      {required String labelText, required Icon icon}) {
    return InputDecoration(
      labelText: labelText,
      suffixIcon: icon,
      enabledBorder: const UnderlineInputBorder(
        borderSide: BorderSide(color: AppColor.yellow, width: 7),
        borderRadius: BorderRadius.all(
          Radius.circular(20.0),
        ),
      ),
      focusedBorder: const UnderlineInputBorder(
        borderSide: BorderSide(color: AppColor.yellow, width: 7),
        borderRadius: BorderRadius.all(
          Radius.circular(20.0),
        ),
      ),
      contentPadding: const EdgeInsets.all(10),
    );
  }
}

class SearchList extends StatelessWidget {
  final SearchedProvider? provider;
  const SearchList({Key? key, required this.provider}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: provider?.productList.length,
        itemBuilder: (context, index) {
          return ListTile(
            selectedColor: Colors.amber,
            onTap: () {
              _nameProductController.text =
                  provider?.productList[index]['name'];
              selectedId = provider!.productList[index]['id'].toString();

              provider!.resetProductList();
              provider!.callAction(0.toDouble());
            },
            title: Text(provider?.productList[index]['name']),
          );
        },
      ),
    );
  }
}
