import 'package:calories/global/navigator/main_navigation.dart';
import 'package:calories/global/utils/snack_bar.dart';
import 'package:calories/global/widgets/errors/auth_error.dart';
import 'package:calories/main.dart';
import 'package:calories/modules/new_record/record_controller.dart';
import 'package:flutter/material.dart';

class SearchedProvider extends ChangeNotifier {
  List<dynamic> productList = [];
  String error = "";
  RecordController controller = RecordController();

  Function _action = () {};
  getProductList() {
    notifyListeners();
    // print(productList);
    return productList;
  }

  updateProductList(query, [context = null]) async {
    try {
      productList = await controller.searchProducts(query);
    } on AuthException catch (e) {
      Navigator.of(context)
          .pushReplacementNamed(MainNavigationRouteNames.login);
    } catch (e) {
      error = e.toString();
      callAction(0.toDouble());
      rootScaffoldMessengerKey.currentState?.showSnackBar(
        createSnackBar(showedText: e.toString(), result: false),
      );
    }

    notifyListeners();
  }

  setAction(callback) {
    _action = callback;
  }

  callAction([params = null]) {
    _action(params);
  }

  resetProductList() {
    productList = [];
    notifyListeners();
  }
}
