import 'package:calories/global/endpoints/api.dart';
import 'package:flutter/material.dart';

class RecordController {
  Api api = Api();
  late List<Map<String, dynamic>> _categories;
  final gramsTextController = TextEditingController();
  final nameTextController = TextEditingController();
  final kcalTextController = TextEditingController();
  final proteinsTextController = TextEditingController();
  final fatsTextController = TextEditingController();
  final carbsTextController = TextEditingController();

  Future createShortRecord(selectedId, date) async {
    var body = {
      'product_id': selectedId,
      'grams': gramsTextController.text,
      'day': date,
    };
    return await api.createRecord(body);
  }

  Future createDetailedRecord(Map<String, dynamic> newProduct) async {
    return await api.createRecord(newProduct);
  }

  Future getCategories() async {
    var listOfCategory = await api.getCategories();
    listOfCategory = listOfCategory['categories'];
    _categories = [];
    for (var i = 0; i < listOfCategory.length; i++) {
      Map<String, dynamic> item = {};
      item['id'] = listOfCategory[i]['id'];
      item['name'] = listOfCategory[i]['name'];

      _categories.add(item);
    }

    return _categories;
  }

  Future searchProducts(String query) async {
    var response = await api.searchProduct(query);
    return response['products']['data'];
  }
}
