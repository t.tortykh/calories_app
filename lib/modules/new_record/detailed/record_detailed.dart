import 'package:calories/global/utils/snack_bar.dart';
import 'package:calories/global/models/new_product/new_product_model.dart';
import 'package:calories/global/navigator/main_navigation.dart';
import 'package:calories/global/widgets/errors/auth_error.dart';
import 'package:calories/lib/global/themes/resources/resources.dart';
import 'package:calories/main.dart';
import 'package:calories/modules/new_record/record_controller.dart';
import 'package:calories/global/themes/theme/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

var _finalDate;
var newRecord = NewRecord();
Map<String, dynamic> recordData = {};

final RecordController controller = RecordController();

class RecordDetailed extends StatelessWidget {
  RecordDetailed({super.key});

  static const numKeyboard = TextInputType.numberWithOptions(decimal: true);

  // final AddNewProductApi _newProductApi = AddNewProductApi();

  final GlobalKey<FormState> _formKeyNameNewProd = GlobalKey<FormState>();
  final GlobalKey<FormState> _formKeyCateg = GlobalKey<FormState>();
  final GlobalKey<FormState> _formKeyKcal = GlobalKey<FormState>();
  final GlobalKey<FormState> _formKeyProteins = GlobalKey<FormState>();
  final GlobalKey<FormState> _formKeyFats = GlobalKey<FormState>();
  final GlobalKey<FormState> _formKeyCarbs = GlobalKey<FormState>();
  final GlobalKey<FormState> _formKeyGrams = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(title: const Text('Add New Product')),
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
                image: DecorationImage(
              opacity: 1,
              image: AssetImage(AppImages.wallpaper_5),
              fit: BoxFit.fitHeight,
            )),
          ),
          SafeArea(
            child: SingleChildScrollView(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10),
                child: Column(
                  children: [
                    Form(
                      key: _formKeyNameNewProd,
                      child: TextFormField(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        textInputAction: TextInputAction.next,
                        decoration: _decorationTextField('Name of product'),
                        controller: controller.nameTextController,
                        validator: ((value) {
                          if (value == null || value == '') {
                            return 'Provide the name of the product';
                          } else {
                            return null;
                          }
                        }),
                      ),
                    ),
                    const SizedBox(height: 25),
                    CategoryDropDown(formKeyCateg: _formKeyCateg),
                    const SizedBox(height: 25),
                    Form(
                      key: _formKeyKcal,
                      child: TextFormField(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        textInputAction: TextInputAction.next,
                        decoration: _decorationTextField('Kcal'),
                        keyboardType: TextInputType.datetime,
                        controller: controller.kcalTextController,
                        inputFormatters: [
                          FilteringTextInputFormatter.allow((RegExp("[.0-9]")))
                        ],
                        validator: ((value) {
                          if (value == null || value == '') {
                            return 'Required field';
                          }
                          if (!RegExp(r'^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$')
                              .hasMatch(value)) {
                            return 'Please, provide the correct data';
                          } else {
                            return null;
                          }
                        }),
                      ),
                    ),
                    const SizedBox(height: 25),
                    Form(
                      key: _formKeyProteins,
                      child: TextFormField(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        textInputAction: TextInputAction.next,
                        decoration: _decorationTextField('Proteins'),
                        keyboardType: TextInputType.datetime,
                        inputFormatters: [
                          FilteringTextInputFormatter.allow((RegExp("[.0-9]")))
                        ],
                        controller: controller.proteinsTextController,
                        validator: ((value) {
                          if (value == null || value == '') {
                            return 'Required field';
                          }
                          if (!RegExp(r'^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$')
                              .hasMatch(value)) {
                            return 'Please, provide the correct data';
                          } else {
                            return null;
                          }
                        }),
                      ),
                    ),
                    const SizedBox(height: 25),
                    Form(
                      key: _formKeyFats,
                      child: TextFormField(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        textInputAction: TextInputAction.next,
                        decoration: _decorationTextField('Fats'),
                        keyboardType: TextInputType.datetime,
                        inputFormatters: [
                          FilteringTextInputFormatter.allow((RegExp("[.0-9]")))
                        ],
                        controller: controller.fatsTextController,
                        validator: ((value) {
                          if (value == null || value == '') {
                            return 'Required field';
                          }
                          if (!RegExp(r'^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$')
                              .hasMatch(value)) {
                            return 'Please, provide the correct data';
                          } else {
                            return null;
                          }
                        }),
                      ),
                    ),
                    const SizedBox(height: 25),
                    Form(
                      key: _formKeyCarbs,
                      child: TextFormField(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        textInputAction: TextInputAction.next,
                        decoration: _decorationTextField('Carbohydrates'),
                        keyboardType: TextInputType.datetime,
                        inputFormatters: [
                          FilteringTextInputFormatter.allow((RegExp("[.0-9]")))
                        ],
                        controller: controller.carbsTextController,
                        validator: ((value) {
                          if (value == null || value == '') {
                            return 'Required field';
                          }
                          if (!RegExp(r'^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$')
                              .hasMatch(value)) {
                            return 'Please, provide the correct data';
                          } else {
                            return null;
                          }
                        }),
                      ),
                    ),
                    const SizedBox(height: 25),
                    Form(
                      key: _formKeyGrams,
                      child: TextFormField(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          textInputAction: TextInputAction.done,
                          decoration:
                              _decorationTextField('Grams you want to eat now'),
                          keyboardType: TextInputType.datetime,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                (RegExp("[.0-9]")))
                          ],
                          controller: controller.gramsTextController,
                          validator: (value) {
                            if (value == null || value == '') {
                              return 'Required field';
                            } else if (value.isNotEmpty) {
                              if (!RegExp(
                                      r'^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$')
                                  .hasMatch(value)) {
                                return 'Please, provide the correct data';
                              }
                            }
                            return null;
                          }),
                    ),
                    const SizedBox(height: 25),
                    const DatePicker(),
                    PostAddProduct(
                      formKeyNameNewProd: _formKeyNameNewProd,
                      formKeyCateg: _formKeyCateg,
                      formKeyKcal: _formKeyKcal,
                      formKeyProteins: _formKeyProteins,
                      formKeyFats: _formKeyFats,
                      formKeyCarb: _formKeyCarbs,
                      // newProductApi: controller,
                      formKeyGrams: _formKeyGrams,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  InputDecoration _decorationTextField(String lableText) {
    return InputDecoration(
      labelText: lableText,
      enabledBorder: const UnderlineInputBorder(
        borderSide: BorderSide(color: AppColor.yellow, width: 7),
        borderRadius: BorderRadius.all(
          Radius.circular(20.0),
        ),
      ),
      focusedBorder: const UnderlineInputBorder(
        borderSide: BorderSide(color: AppColor.yellow, width: 7),
        borderRadius: BorderRadius.all(
          Radius.circular(20.0),
        ),
      ),
      contentPadding: const EdgeInsets.all(5),
    );
  }
}

class PostAddProduct extends StatefulWidget {
  final GlobalKey<FormState> formKeyNameNewProd;
  final GlobalKey<FormState> formKeyCateg;
  final GlobalKey<FormState> formKeyKcal;
  final GlobalKey<FormState> formKeyProteins;
  final GlobalKey<FormState> formKeyFats;
  final GlobalKey<FormState> formKeyCarb;
  final GlobalKey<FormState> formKeyGrams;
  const PostAddProduct({
    super.key,
    required this.formKeyNameNewProd,
    required this.formKeyCateg,
    required this.formKeyKcal,
    required this.formKeyProteins,
    required this.formKeyFats,
    required this.formKeyCarb,
    required this.formKeyGrams,
  });

  @override
  State<PostAddProduct> createState() => _PostAddProductState();
}

class _PostAddProductState extends State<PostAddProduct> {
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: _buttonStyle(),
      onPressed: () async {
        widget.formKeyNameNewProd.currentState?.validate();
        widget.formKeyCateg.currentState?.validate();
        widget.formKeyKcal.currentState?.validate();
        widget.formKeyProteins.currentState?.validate();
        widget.formKeyFats.currentState?.validate();
        widget.formKeyCarb.currentState?.validate();
        widget.formKeyGrams.currentState?.validate();
        if (widget.formKeyNameNewProd.currentState!.validate() &&
            widget.formKeyCateg.currentState!.validate() &&
            widget.formKeyKcal.currentState!.validate() &&
            widget.formKeyProteins.currentState!.validate() &&
            widget.formKeyFats.currentState!.validate() &&
            widget.formKeyCarb.currentState!.validate() &&
            widget.formKeyGrams.currentState!.validate()) {
          recordData['name'] = controller.nameTextController.text;
          recordData['calories'] = controller.kcalTextController.text;
          recordData['proteins'] = controller.proteinsTextController.text;
          recordData['fats'] = controller.fatsTextController.text;
          recordData['carbs'] = controller.carbsTextController.text;
          recordData['grams'] = controller.gramsTextController.text;
          recordData['day'] = _finalDate;

          var responseMessage = '';
          try {
            var response = await controller.createDetailedRecord(recordData);
            responseMessage = response['message'] ?? 'Success';
            setState(() {
              controller.nameTextController.clear();
              controller.kcalTextController.clear();
              controller.proteinsTextController.clear();
              controller.fatsTextController.clear();
              controller.carbsTextController.clear();
              controller.gramsTextController.clear();
              rootScaffoldMessengerKey.currentState?.showSnackBar(
                createSnackBar(
                    showedText: 'You just added new product', result: true),
              );
            });
            Navigator.pop(context);
          } on AuthException catch (e) {
            Navigator.of(context)
                .pushReplacementNamed(MainNavigationRouteNames.login);
          } catch (e) {
            rootScaffoldMessengerKey.currentState?.showSnackBar(
                createSnackBar(showedText: e.toString(), result: false));
            // SnackBar(content: Text(e.toString())));
          }
        }
      },
      child: const Text(
        'Add New Product',
        style: TextStyle(fontSize: 33, color: Colors.black),
      ),
    );
  }
}

ButtonStyle _buttonStyle() {
  return ButtonStyle(
      padding: MaterialStateProperty.all<EdgeInsets>(
          const EdgeInsets.symmetric(vertical: 10, horizontal: 30)),
      backgroundColor: MaterialStateProperty.all(AppColor.green),
      shape: MaterialStateProperty.all(RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18.0),
      )));
}

class DatePicker extends StatefulWidget {
  const DatePicker({super.key});

  @override
  State<DatePicker> createState() => _DatePickerState();
}

class _DatePickerState extends State<DatePicker> {
  DateTime _selectedDate = DateTime.now();

  void _presentDatePicker() {
    showDatePicker(
            context: context,
            initialDate: DateTime.now(),
            firstDate: DateTime(2023),
            lastDate: DateTime.now())
        .then((pickedDate) {
      if (pickedDate == null) {
        return;
      }
      setState(() {
        _selectedDate = pickedDate;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    _finalDate = DateFormat('y-MM-dd').format(_selectedDate);
    final _showingDate = DateFormat('d MMMM').format(_selectedDate);
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
              _selectedDate == null
                  ? 'No date choosen!'
                  : 'Date: $_showingDate',
              style: const TextStyle(color: AppColor.tealDark)),
          TextButton(
            onPressed: _presentDatePicker,
            child: const Text(
              'Change Date',
              style: TextStyle(color: AppColor.coral),
            ),
          ),
        ],
      ),
    );
  }
}

class CategoryDropDown extends StatefulWidget {
  final formKeyCateg;
  const CategoryDropDown({required this.formKeyCateg, super.key});

  @override
  State<CategoryDropDown> createState() => _CategoryDropDownState();
}

class _CategoryDropDownState extends State<CategoryDropDown> {
  var dropdown = [];
  List<dynamic>? _categoriesList;
  bool _isLoading = true;

  @override
  void initState() {
    showCategories();
    super.initState();
  }

  showCategories() async {
    _categoriesList = await controller.getCategories();
    setState(() {
      _isLoading = false;
    });
  }

  var dropdownvalue;
  @override
  Widget build(BuildContext context) {
    return _isLoading
        ? const CircularProgressIndicator()
        : Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5.0),
            child: Form(
              key: widget.formKeyCateg,
              child: DropdownButtonFormField(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator: (value) => value == null ? 'Field required' : null,
                decoration: const InputDecoration(
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: AppColor.yellow, width: 3),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: AppColor.yellow, width: 3),
                  ),
                ),
                hint: const Text('Choose Category'),
                items: _categoriesList?.map((item) {
                  return DropdownMenuItem(
                    value: item['id'].toString(),
                    child: Text(item['name'].toString()),
                  );
                }).toList(),
                onChanged: (newVal) {
                  setState(() {
                    dropdownvalue = newVal;
                    recordData['category_id'] = dropdownvalue;
                  });
                },
                value: dropdownvalue,
              ),
            ),
          );
  }
}
