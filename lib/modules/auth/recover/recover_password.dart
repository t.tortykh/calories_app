import 'package:calories/global/navigator/main_navigation.dart';
import 'package:flutter/material.dart';

class RecoverPassword extends StatefulWidget {
  const RecoverPassword({super.key});

  @override
  State<RecoverPassword> createState() => _RecoverPasswordState();
}

class _RecoverPasswordState extends State<RecoverPassword> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Forgot Password'),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(30.0),
          child: Column(
            children: [
              const Placeholder(fallbackHeight: 150),
              const SizedBox(height: 50),
              const Text(
                'Please enter your e-mail to recive a verification code',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16),
                maxLines: 3,
              ),
              const SizedBox(height: 25),
              const TextField(
                decoration: InputDecoration(
                  suffixIcon: Icon(Icons.email_outlined),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.green),
                    borderRadius: BorderRadius.all(
                      Radius.circular(20.0),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.amber),
                    borderRadius: BorderRadius.all(
                      Radius.circular(20.0),
                    ),
                  ),
                  contentPadding: EdgeInsets.all(10),
                  labelText: 'Enter your e-mail',
                ),
              ),
              const SizedBox(height: 50),
              ElevatedButton(
                onPressed: () {
                  Navigator.of(context)
                      .pushNamed(MainNavigationRouteNames.verifyMail);
                },
                style: ButtonStyle(
                    padding: MaterialStateProperty.all<EdgeInsets>(
                        const EdgeInsets.symmetric(
                            vertical: 10, horizontal: 30)),
                    backgroundColor: MaterialStateProperty.all(Colors.amber),
                    shape: MaterialStateProperty.all(RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ))),
                child: const Text(
                  'Send',
                  style: TextStyle(fontSize: 33),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
