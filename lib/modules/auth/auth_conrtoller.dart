import 'dart:async';

import 'package:calories/global/endpoints/api.dart';
import 'package:calories/global/providers/session_data_provider/session_data_provider.dart';
import 'package:calories/global/navigator/main_navigation.dart';
import 'package:flutter/material.dart';

class AuthController {
  final _sessionTokenProvider = SessionDataProvider();
  Api api = Api();
  final nameTextController = TextEditingController();
  final emailTextController = TextEditingController();
  final passwordTextController = TextEditingController();
  final passwordCheckTextController = TextEditingController();
  final loginTextController = TextEditingController();
  // text: 'test1@test.com'
  final passwordLoginTextController = TextEditingController();
  // text: 'NiceNice39'

  Future register(context) async {
    var body = {
      'name': nameTextController.text,
      'email': emailTextController.text,
      'password': passwordTextController.text,
      'password_confirmation': passwordCheckTextController.text,
    };

    return api.register(body);
  }

  Future login(context) async {
    var body = {
      'email': loginTextController.text,
      'password': passwordLoginTextController.text,
    };
    var responseData = await api.login(body);
    if (responseData['success']) {
      await _sessionTokenProvider.setToken(responseData['token']);
      unawaited(Navigator.of(context)
          .pushReplacementNamed(MainNavigationRouteNames.mainScreen));
    }

    return responseData;
  }

  handleErrorResponse(arr, res, err) {
    for (var i = 0; i <= arr.length - 1; i++) {
      if (res[arr[i]] != null && res[arr[i]].length != 0) {
        err[i] = res[arr[i]][0];
      }
    }

    return err;
  }
}
