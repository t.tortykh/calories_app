import 'package:calories/global/navigator/main_navigation.dart';
import 'package:calories/global/utils/snack_bar.dart';
import 'package:calories/lib/global/themes/resources/resources.dart';
import 'package:calories/main.dart';
import 'package:calories/modules/auth/auth_conrtoller.dart';
import 'package:flutter/material.dart';

class CreateNewAcc extends StatelessWidget {
  const CreateNewAcc({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: const Text('Create accaunt'),
      ),
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
                image: DecorationImage(
              opacity: 1,
              image: AssetImage(AppImages.wallpaper_4),
              fit: BoxFit.fitHeight,
            )),
          ),
          const SafeArea(
            child: NewAccFields(),
          ),
        ],
      ),
    );
  }
}

class NewAccFields extends StatefulWidget {
  const NewAccFields({super.key});

  @override
  State<NewAccFields> createState() => _NewAccFieldsState();
}

class _NewAccFieldsState extends State<NewAccFields> {
  AuthController authController = AuthController();

  bool _obscurePassOne = true;
  bool _obscurePassTwo = true;

  void _showObscurePass() {
    setState(() {
      _obscurePassOne = !_obscurePassOne;
    });
  }

  void _showObscurePassCheck() {
    setState(() {
      _obscurePassTwo = !_obscurePassTwo;
    });
  }

  var validation = AutovalidateMode.disabled;

  final GlobalKey<FormState> _formKeyName = GlobalKey<FormState>();
  final GlobalKey<FormState> _formKeyEmail = GlobalKey<FormState>();
  final GlobalKey<FormState> _formKeyPass = GlobalKey<FormState>();
  final GlobalKey<FormState> _formKeyPassCheck = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(30.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildNameField(),
          const SizedBox(height: 23),
          _buildEmailField(),
          const SizedBox(height: 23),
          _buildPassField(),
          const SizedBox(height: 23),
          _buildPassCheckField(),
          // const _ErrorMessageWidget(),
          const SizedBox(height: 60),
          _PostButton(
            formKeyName: _formKeyName,
            formKeyEmail: _formKeyEmail,
            formKeyPass: _formKeyPass,
            formKeyPassCheck: _formKeyPassCheck,
            authController: authController,
          )
        ],
      ),
    );
  }

  Form _buildPassCheckField() {
    return Form(
      key: _formKeyPassCheck,
      child: TextFormField(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        controller: authController.passwordCheckTextController,
        obscureText: _obscurePassTwo,
        decoration: _inputDecorationsPassCheck(
          lableText: 'Repeat your password',
          sufIcon: const Icon(Icons.visibility),
        ),
        textInputAction: TextInputAction.done,
        validator: (value) {
          if (value == null || value == '') {
            return 'Fill your password';
          }
          if (value != authController.passwordTextController.text) {
            return 'Passwords are different';
          } else {
            return null;
          }
        },
      ),
    );
  }

  Form _buildPassField() {
    return Form(
      key: _formKeyPass,
      child: TextFormField(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        controller: authController.passwordTextController,
        obscureText: _obscurePassOne,
        decoration: _inputDecorations(
          lableText: 'Type your password',
          sufIcon: const Icon(Icons.visibility),
        ),
        textInputAction: TextInputAction.next,
        validator: (value) {
          if (value == null || value == '') {
            return 'Fill your password';
          } else {
            return null;
          }
        },
      ),
    );
  }

  Form _buildEmailField() {
    return Form(
      key: _formKeyEmail,
      child: TextFormField(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        controller: authController.emailTextController,
        decoration: _inputDecorations(
          lableText: 'Email',
          sufIcon: const Icon(Icons.email_outlined),
        ),
        keyboardType: TextInputType.emailAddress,
        textInputAction: TextInputAction.next,
        validator: (value) {
          if (value == null || value == '') {
            return 'Fill your Email';
          } else if (!RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$')
              .hasMatch(value)) {
            return 'Check your Email';
          } else {
            return null;
          }
        },
      ),
    );
  }

  Form _buildNameField() {
    return Form(
      key: _formKeyName,
      child: TextFormField(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        controller: authController.nameTextController,
        decoration: _inputDecorations(
          lableText: 'Your name',
          sufIcon: const Icon(Icons.person_outline_sharp),
        ),
        textInputAction: TextInputAction.next,
        validator: (value) {
          if (value == null || value == '') {
            return 'Fill your name';
          } else {
            return null;
          }
        },
      ),
    );
  }

  InputDecoration _inputDecorations({
    required String lableText,
    required Icon sufIcon,
  }) {
    return InputDecoration(
      suffixIcon: sufIcon.toString() ==
                  const Icon(Icons.email_outlined).toString() ||
              sufIcon.toString() ==
                  const Icon(Icons.person_outline_sharp).toString()
          ? sufIcon
          : IconButton(
              icon: Icon(
                  _obscurePassOne ? Icons.visibility_off : Icons.visibility),
              onPressed: _showObscurePass,
            ),
      enabledBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.green),
        borderRadius: BorderRadius.all(
          Radius.circular(20.0),
        ),
      ),
      focusedBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.amber),
        borderRadius: BorderRadius.all(
          Radius.circular(20.0),
        ),
      ),
      contentPadding: const EdgeInsets.all(10),
      labelText: lableText,
    );
  }

  InputDecoration _inputDecorationsPassCheck({
    required String lableText,
    required Icon sufIcon,
  }) {
    return InputDecoration(
      suffixIcon: IconButton(
        icon: Icon(_obscurePassTwo ? Icons.visibility_off : Icons.visibility),
        onPressed: _showObscurePassCheck,
      ),
      enabledBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.green),
        borderRadius: BorderRadius.all(
          Radius.circular(20.0),
        ),
      ),
      focusedBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.amber),
        borderRadius: BorderRadius.all(
          Radius.circular(20.0),
        ),
      ),
      contentPadding: const EdgeInsets.all(10),
      labelText: lableText,
    );
  }
}

class _PostButton extends StatelessWidget {
  const _PostButton({
    Key? key,
    required GlobalKey<FormState> formKeyName,
    required GlobalKey<FormState> formKeyEmail,
    required GlobalKey<FormState> formKeyPass,
    required GlobalKey<FormState> formKeyPassCheck,
    required this.authController,
  })  : _formKeyName = formKeyName,
        _formKeyEmail = formKeyEmail,
        _formKeyPass = formKeyPass,
        _formKeyPassCheck = formKeyPassCheck,
        super(key: key);

  final GlobalKey<FormState> _formKeyName;
  final GlobalKey<FormState> _formKeyEmail;
  final GlobalKey<FormState> _formKeyPass;
  final GlobalKey<FormState> _formKeyPassCheck;
  final AuthController authController;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () async {
        _formKeyName.currentState?.validate();
        _formKeyEmail.currentState?.validate();
        _formKeyPass.currentState?.validate();
        _formKeyPassCheck.currentState?.validate();
        if (_formKeyName.currentState!.validate() &&
            _formKeyEmail.currentState!.validate() &&
            _formKeyPass.currentState!.validate() &&
            _formKeyPassCheck.currentState!.validate()) {
          try {
            await authController.register(context);
            Navigator.of(context)
                .pushReplacementNamed(MainNavigationRouteNames.login);
          } catch (e) {
            rootScaffoldMessengerKey.currentState?.showSnackBar(
                createSnackBar(showedText: e.toString(), result: false));
          }
        }
      },
      style: ButtonStyle(
          padding: MaterialStateProperty.all<EdgeInsets>(
              const EdgeInsets.symmetric(vertical: 10, horizontal: 30)),
          backgroundColor: MaterialStateProperty.all(Colors.amber),
          shape: MaterialStateProperty.all(RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
          ))),
      child: const Text(
        'Submit',
        style: TextStyle(fontSize: 33, fontWeight: FontWeight.bold),
      ),
    );
  }
}
