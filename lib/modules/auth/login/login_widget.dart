import 'package:calories/global/utils/snack_bar.dart';
import 'package:calories/main.dart';
import 'package:calories/modules/auth/auth_conrtoller.dart';
import 'package:calories/global/navigator/main_navigation.dart';
import 'package:calories/lib/global/themes/resources/resources.dart';
import 'package:flutter/material.dart';

class LoginWidget extends StatefulWidget {
  const LoginWidget({super.key});

  @override
  State<LoginWidget> createState() => _LoginWidgetState();
}

class _LoginWidgetState extends State<LoginWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(),
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
                image: DecorationImage(
              opacity: 1,
              image: AssetImage(AppImages.wallpaper_4),
              fit: BoxFit.fitHeight,
            )),
          ),
          SafeArea(
            child: Column(children: const [
              _Header(),
              SizedBox(height: 100),
              _SignInForm(),
              SizedBox(height: 20),
            ]),
          ),
        ],
      ),
    );
  }
}

class _SignInForm extends StatefulWidget {
  const _SignInForm({super.key});

  @override
  State<_SignInForm> createState() => _SignInFormState();
}

class _SignInFormState extends State<_SignInForm> {
  bool _obscureTextPass = true;
  late Map<String, dynamic> errors;
  var mailError = null;
  var passwordError = null;

  AuthController authController = AuthController();

  void _showObscureText() {
    setState(() {
      _obscureTextPass = !_obscureTextPass;
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(children: [
        TextFormField(
          controller: authController.loginTextController,
          keyboardType: TextInputType.emailAddress,
          onChanged: (value) {
            setState(() {
              mailError = null;
            });
          },
          decoration: textFieldDecorator(
            errorText: mailError,
            icon: const Icon(Icons.person_outline_sharp),
            lableText: 'Email',
            sufIcon: const Icon(Icons.email_outlined),
          ),
          textInputAction: TextInputAction.next,
        ),
        const SizedBox(height: 23),
        TextField(
          controller: authController.passwordLoginTextController,
          decoration: textFieldDecorator(
              errorText: passwordError,
              icon: const Icon(Icons.lock_outline_sharp),
              lableText: 'Password',
              sufIcon: const Icon(Icons.visibility_off)),
          obscureText: _obscureTextPass,
          onChanged: (value) {
            setState(() {
              passwordError = null;
            });
          },
          textInputAction: TextInputAction.done,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            // TextButton(
            //     onPressed: () {
            //       Navigator.of(context)
            //           .pushNamed(MainNavigationRouteNames.enterMailToVerify);
            //     },
            //     child: const Text(
            //       'Forgot your password?',
            //       style: TextStyle(
            //         color: Colors.grey,
            //         decoration: TextDecoration.underline,
            //       ),
            //     )),
            const SizedBox(height: 70),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                const Text('Sign in',
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                    )),
                const SizedBox(width: 15),
                ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.amber),
                      shape: MaterialStateProperty.all(RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      ))),
                  onPressed: () async {
                    try {
                      var res = await authController.login(context);
                      if (!res['success']) {
                        setState(() {
                          if (res['error'] == 'validation_error') {
                            if (res['errors']['email'] != null &&
                                res['errors']['email'].length != 0) {
                              mailError = res['errors']['email'][0];
                            }
                            if (res['errors']['password'] != null &&
                                res['errors']['password'].length != 0) {
                              passwordError = res['errors']['password'][0];
                            }
                          }
                        });
                      } else {
                        rootScaffoldMessengerKey.currentState?.showSnackBar(
                            createSnackBar(
                                showedText: 'Glad to see you again :)',
                                result: true));
                        return;
                      }
                    } catch (e) {
                      rootScaffoldMessengerKey.currentState?.showSnackBar(
                          createSnackBar(
                              showedText: e.toString(), result: false));
                    }
                  },
                  child: const Icon(Icons.arrow_forward),
                )
              ],
            ),
            const SizedBox(height: 50),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text('Don\'t have an accaunt?',
                    style: TextStyle(
                      fontSize: 18,
                    )),
                TextButton(
                    onPressed: () {
                      Navigator.of(context)
                          .pushNamed(MainNavigationRouteNames.newUser);
                    },
                    child: const Text(
                      'Create',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                          decoration: TextDecoration.underline,
                          color: Colors.black),
                    ))
              ],
            )
          ],
        )
      ]),
    );
  }

  InputDecoration textFieldDecorator({
    required String? errorText,
    required Icon icon,
    required String lableText,
    required Icon sufIcon,
  }) {
    return InputDecoration(
      errorText: errorText,
      icon: icon,
      suffixIcon: sufIcon.toString() ==
              const Icon(Icons.email_outlined).toString()
          ? sufIcon
          : IconButton(
              icon: Icon(
                  _obscureTextPass ? Icons.visibility_off : Icons.visibility),
              onPressed: _showObscureText,
            ),
      enabledBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.green),
        borderRadius: BorderRadius.all(
          Radius.circular(20.0),
        ),
      ),
      focusedBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.amber),
        borderRadius: BorderRadius.all(
          Radius.circular(20.0),
        ),
      ),
      contentPadding: const EdgeInsets.all(10),
      labelText: lableText,
    );
  }
}

// class _ErrorMessageWidget extends StatelessWidget {
//   const _ErrorMessageWidget({super.key});

//   @override
//   Widget build(BuildContext context) {
//     String errorMessage =  ;
//     if (errorMessage == null) return const SizedBox.shrink();
//     return Padding(
//       padding: const EdgeInsets.only(bottom: 20),
//       child: Text(
//         errorMessage,
//         style: const TextStyle(color: Colors.red, fontSize: 18),
//       ),
//     );
//   }
// }

class _Header extends StatelessWidget {
  const _Header({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(children: const [
        Text(
          'Hello',
          style: TextStyle(fontSize: 110),
        ),
        Text(
          'Sign in to your account',
          style: TextStyle(fontSize: 23),
        )
      ]),
    );
  }
}
