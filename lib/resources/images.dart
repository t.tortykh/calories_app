part of 'resources.dart';

class Images {
  Images._();

  static const String addImage = 'lib/lib/global/themes/images/add-image.png';
  static const String appIcon = 'lib/lib/global/themes/images/app_icon.png';
  static const String calculator =
      'lib/lib/global/themes/images/calculator.png';
  static const String calendar = 'lib/lib/global/themes/images/calendar.png';
  static const String calories = 'lib/lib/global/themes/images/calories.png';
  static const String cancel = 'lib/lib/global/themes/images/cancel.png';
  static const String check = 'lib/lib/global/themes/images/check.png';
  static const String cross = 'lib/lib/global/themes/images/cross.png';
  static const String dish = 'lib/lib/global/themes/images/dish.png';
  static const String energyBar = 'lib/lib/global/themes/images/energy-bar.png';
  static const String fat = 'lib/lib/global/themes/images/fat.png';
  static const String fruitsVegetables =
      'lib/lib/global/themes/images/fruits_ vegetables.png';
  static const String kcal = 'lib/lib/global/themes/images/kcal.png';
  static const String luchTime = 'lib/lib/global/themes/images/luch-time.png';
  static const String smiling = 'lib/lib/global/themes/images/smiling.png';
  static const String wallpaper1 =
      'lib/lib/global/themes/images/wallpaper_1.jpg';
  static const String wallpaper2 =
      'lib/lib/global/themes/images/wallpaper_2.jpg';
  static const String wallpaper3 =
      'lib/lib/global/themes/images/wallpaper_3.jpg';
  static const String wallpaper4 =
      'lib/lib/global/themes/images/wallpaper_4.jpg';
  static const String wallpaper5 =
      'lib/lib/global/themes/images/wallpaper_5.jpg';
  static const String whey = 'lib/lib/global/themes/images/whey.png';
}
