import 'dart:io';
import 'package:calories/global/utils/http_client.dart';

class Api {
  // BuildContext? context = null;
  HttpClientApi clientApi = HttpClientApi();
  var errorResponse = [];

  // Api([context = null]) {
  //   clientApi.context = context;
  // }

  getCategories() async {
    return await clientApi.sendRequest(url: '/categories', method: 'get');
  }

  getProducts([page = '1', id]) async {
    return await clientApi.sendRequest(
        url: '/products?category_id=$id&page=$page', method: 'get');
  }

  deleteProduct(body) async {
    return await clientApi.sendRequest(
        url: '/product', method: 'delete', body: body);
  }

  editProduct(body) async {
    return await clientApi.sendRequest(
        url: '/product/edit', method: 'post', body: body);
  }

  getHistory(page) async {
    return await clientApi.sendRequest(
        url: '/history?page=$page', method: 'get');
  }

  getStatistics(period) async {
    return await clientApi.sendRequest(
        url: '/statistics?period=$period', method: 'get');
  }

  getUserData() async {
    return await clientApi.sendRequest(url: '/user', method: 'get');
  }

  createRecord(body) async {
    return await clientApi.sendRequest(
        url: '/record', method: 'post', body: body);
  }

  editRecord(body) async {
    return await clientApi.sendRequest(
        url: '/record/edit', method: 'post', body: body);
  }

  deleteRecord(body) async {
    return await clientApi.sendRequest(
        url: '/record', method: 'delete', body: body);
  }

  uploadUserAvatar(File image) async {
    return await clientApi.mediaRequest('/user/change', image);
  }

  calculateCalories(body) async {
    return await clientApi.sendRequest(
        url: '/calculate', method: 'post', body: body);
  }

  searchProduct(query) async {
    return await clientApi.sendRequest(
        url: '/search?query=$query', method: 'get');
  }

  register(body) async {
    return await clientApi.sendRequest(
        url: '/register', method: 'post', body: body);
  }

  login(body) async {
    return await clientApi.sendRequest(
        url: '/login', method: 'post', body: body);
  }
}
