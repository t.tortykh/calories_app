import 'package:calories/global/themes/theme/app_colors.dart';
import 'package:calories/lib/global/themes/resources/resources.dart';
import 'package:flutter/material.dart';

class PopUpWindow extends StatefulWidget {
  String responseMessage = '';
  bool isLoading = false;
  Function action = () {};
  PopUpWindow(
      {required this.responseMessage,
      required this.isLoading,
      required this.action,
      super.key});

  @override
  State<PopUpWindow> createState() => _PopUpWindowState();
}

class _PopUpWindowState extends State<PopUpWindow> {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
      decoration: _boxDecoration(),
      child: SimpleDialog(
        shape: _shapeDecoration(),
        children: [
          widget.isLoading
              ? const CircularProgressIndicator()
              : (widget.responseMessage == 'Success'
                  ? Image.asset(AppImages.check)
                  : Image.asset(AppImages.cancel)),
          const SizedBox(height: 10),
          Text(
            widget.responseMessage,
            textAlign: TextAlign.center,
          ),
          ElevatedButton(
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(AppColor.tealDark),
                  shape: MaterialStateProperty.all(RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(40.0),
                  ))),
              onPressed: () {
                widget.action();
              },
              child: Text(
                widget.responseMessage == 'Success'
                    ? 'Ok! Add a new one!'
                    : 'Got it!',
                style: const TextStyle(fontSize: 24),
              ))
        ],
      ),
    ));
  }

  RoundedRectangleBorder _shapeDecoration() {
    return const RoundedRectangleBorder(
        side: BorderSide(color: Colors.black, width: 4),
        borderRadius: BorderRadius.all(Radius.circular(35)));
  }

  BoxDecoration _boxDecoration() {
    return BoxDecoration(boxShadow: [
      BoxShadow(
          color: Colors.black.withOpacity(0.15),
          blurRadius: 10,
          offset: const Offset(-2, 2)),
    ]);
  }
}
