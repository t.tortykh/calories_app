import 'dart:math';

import 'package:flutter/material.dart';

class PieDiagram extends CustomPainter {
  final double percentCarbs;
  final double percentProteins;
  final double percentFats;

  PieDiagram({
    required this.percentCarbs,
    required this.percentProteins,
    required this.percentFats,
  });

  @override
  void paint(Canvas canvas, Size size) {
    //circle -> height = const (all calculations depends on heigh)

    final double spaceBeetweenCircles = calculateSpaceBetweenCircles(size);
    final double widthStroke = calculateWidthStroke(size);

    drawBackground(canvas, size);

    drawCarbsLeftLine(widthStroke, canvas, spaceBeetweenCircles, size);
    drawCarbsLine(widthStroke, canvas, spaceBeetweenCircles, size);

    drawProteinLeftLine(widthStroke, canvas, spaceBeetweenCircles, size);
    drawProteinLine(widthStroke, canvas, spaceBeetweenCircles, size);

    drawFatsLeftLine(widthStroke, canvas, spaceBeetweenCircles, size);
    drawFatsLine(widthStroke, canvas, spaceBeetweenCircles, size);
  }

  void drawFatsLine(double widthStroke, Canvas canvas,
      double spaceBeetweenCircles, Size size) {
    final paint = Paint();
    paint.color = Colors.blue;
    paint.style = PaintingStyle.stroke;
    paint.strokeWidth = widthStroke;
    paint.strokeCap = StrokeCap.round;

    canvas.drawArc(
      Offset(
            (spaceBeetweenCircles * 3) / 2,
            (spaceBeetweenCircles * 3) / 2,
          ) &
          Size(size.width - spaceBeetweenCircles * 3,
              size.height - spaceBeetweenCircles * 3),
      0 - pi / 2,
      2 * pi * percentFats,
      false,
      paint,
    );
  }

  void drawFatsLeftLine(double widthStroke, Canvas canvas,
      double spaceBeetweenCircles, Size size) {
    final paint = Paint();
    paint.color = const Color.fromARGB(255, 142, 194, 237);
    paint.style = PaintingStyle.stroke;
    paint.strokeWidth = widthStroke;

    canvas.drawArc(
      Offset(
            (spaceBeetweenCircles * 3) / 2,
            (spaceBeetweenCircles * 3) / 2,
          ) &
          Size(size.width - spaceBeetweenCircles * 3,
              size.height - spaceBeetweenCircles * 3),
      pi * 2 * percentFats - (pi / 2),
      2 * pi * (1.0 - percentFats),
      false,
      paint,
    );
  }

  void drawProteinLine(double widthStroke, Canvas canvas,
      double spaceBeetweenCircles, Size size) {
    final paint = Paint();
    paint.color = Colors.red;
    paint.style = PaintingStyle.stroke;
    paint.strokeWidth = widthStroke;
    paint.strokeCap = StrokeCap.round;

    canvas.drawArc(
      Offset(spaceBeetweenCircles, spaceBeetweenCircles) &
          Size(size.width - spaceBeetweenCircles * 2,
              size.height - spaceBeetweenCircles * 2),
      0 - pi / 2,
      2 * pi * percentProteins,
      false,
      paint,
    );
  }

  void drawProteinLeftLine(double widthStroke, Canvas canvas,
      double spaceBeetweenCircles, Size size) {
    final paint = Paint();
    paint.color = const Color.fromARGB(255, 247, 169, 164);
    paint.style = PaintingStyle.stroke;
    paint.strokeWidth = widthStroke;

    canvas.drawArc(
      Offset(spaceBeetweenCircles, spaceBeetweenCircles) &
          Size(size.width - spaceBeetweenCircles * 2,
              size.height - spaceBeetweenCircles * 2),
      pi * 2 * percentProteins - (pi / 2),
      2 * pi * (1.0 - percentProteins),
      false,
      paint,
    );
  }

  void drawCarbsLine(double widthStroke, Canvas canvas,
      double spaceBeetweenCircles, Size size) {
    final paint = Paint();
    paint.color = const Color.fromARGB(255, 2, 236, 10);
    paint.style = PaintingStyle.stroke;
    paint.strokeWidth = widthStroke;
    paint.strokeCap = StrokeCap.round;

    canvas.drawArc(
      Offset(
            spaceBeetweenCircles / 2,
            spaceBeetweenCircles / 2,
          ) &
          Size(size.width - spaceBeetweenCircles,
              size.height - spaceBeetweenCircles),
      0 - pi / 2,
      2 * pi * percentCarbs,
      false,
      paint,
    );
  }

  void drawCarbsLeftLine(double widthStroke, Canvas canvas,
      double spaceBeetweenCircles, Size size) {
    final paint = Paint();
    paint.color = const Color.fromARGB(255, 191, 228, 194);
    paint.style = PaintingStyle.stroke;
    paint.strokeWidth = widthStroke;

    canvas.drawArc(
      Offset(
            spaceBeetweenCircles / 2,
            spaceBeetweenCircles / 2,
          ) &
          Size(size.width - spaceBeetweenCircles,
              size.height - spaceBeetweenCircles),
      pi * 2 * percentCarbs - (pi / 2),
      2 * pi * (1.0 - percentCarbs),
      false,
      paint,
    );
  }

  void drawBackground(Canvas canvas, Size size) {
    final paint = Paint();
    paint.color = const Color.fromARGB(255, 184, 244, 237);
    paint.style = PaintingStyle.fill;
    canvas.drawCircle(
        Offset(size.width / 2, size.height / 2), size.height / 2, paint);
  }

  double calculateWidthStroke(Size size) {
    const double widthStrokeCoef = 4;
    final double widthStroke = (size.height * widthStrokeCoef) / 100;
    return widthStroke;
  }

  double calculateSpaceBetweenCircles(Size size) {
    const double changingSpaceBetweenCirclesCoefficient = 16;
    final double spaceBeetweenCircles =
        (size.height * changingSpaceBetweenCirclesCoefficient) / 100;
    return spaceBeetweenCircles;
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
