import 'package:calories/modules/auth/register/create_new_acc.dart';
import 'package:calories/modules/auth/login/login_widget.dart';
import 'package:calories/modules/auth/recover/new_password.dart';
import 'package:calories/modules/auth/recover/recover_password.dart';
import 'package:calories/modules/auth/recover/verify_email.dart';

import 'package:calories/modules/history/ui/history_screen.dart';
import 'package:calories/modules/categories/category_list_widget.dart';
import 'package:calories/modules/new_record/detailed/record_detailed.dart';
import 'package:calories/check_later/pie_history/pie_history.dart';
import 'package:calories/modules/main/ui/main_screen_widget.dart';
import 'package:calories/modules/new_record/short/record_short.dart';
import 'package:calories/modules/calculator/ui/calculator_screen.dart';
import 'package:calories/modules/profile/ui/profile_screen.dart';
import 'package:calories/modules/statistics/ui/week_statistic_screen.dart';
import 'package:flutter/material.dart';

abstract class MainNavigationRouteNames {
  static const login = 'login';
  static const newUser = 'login/create_new_acc';
  static const enterMailToVerify = 'login/recover/recover_password';
  static const verifyMail = 'login/recover/verify_email';
  static const createNewPassword = 'login/recover/verify_email/new_password';
  static const mainScreen = '/';
  static const userCalculation = 'user_calculation_screen';
  static const userInfo = '/user_info_screen';
  static const daysHistoryList = '/day_history';
  static const weekStats = '/week_ststistic';
  static const createRecord = '/create_record_form';
  static const newProduct = '/add_new_product';
  static const categoriesScreen = '/food_categories_list_widget';
  static const foodList = '/food_categories_list_widget/food_list_screen';
  static const pieHistory = '/pie_history';
  static const productScreen =
      '/food_categories_list_widget/food_list_screen/product_card';
}

class MainNavigation {
  final route = <String, Widget Function(BuildContext)>{
    MainNavigationRouteNames.login: (context) => const LoginWidget(),
    MainNavigationRouteNames.newUser: (context) => const CreateNewAcc(),
    MainNavigationRouteNames.enterMailToVerify: (context) =>
        const RecoverPassword(),
    MainNavigationRouteNames.verifyMail: (context) => const VerifyEmail(),
    MainNavigationRouteNames.createNewPassword: (context) =>
        const NewPassword(),
    MainNavigationRouteNames.mainScreen: (context) => const MainScreenWidget(),
    MainNavigationRouteNames.userCalculation: (context) =>
        const CalculationScreen(),
    MainNavigationRouteNames.userInfo: (context) => const ProfileScreen(),
    MainNavigationRouteNames.daysHistoryList: (context) =>
        const HistoryScreen(),
    MainNavigationRouteNames.weekStats: (context) =>
        const WeekStatisticScreen(),
    MainNavigationRouteNames.createRecord: (context) => const RecordShort(),
    MainNavigationRouteNames.newProduct: (context) => RecordDetailed(),
    MainNavigationRouteNames.categoriesScreen: (context) =>
        const CategoryListWidget(),
    MainNavigationRouteNames.pieHistory: (context) => const PieHistory(),
  };
}
