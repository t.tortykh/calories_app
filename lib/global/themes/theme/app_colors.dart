import 'package:flutter/material.dart';

abstract class AppColor {
  static const tealLight = Color.fromRGBO(84, 186, 185, 1);

  static const tealDark = Color.fromRGBO(24, 151, 143, 1);
  static const light = Color.fromRGBO(233, 218, 193, 1);
  static const lightest = Color.fromRGBO(247, 236, 222, 1);
  static const yellow = Color.fromRGBO(253, 192, 105, 1);
  static const coral = Color.fromRGBO(239, 90, 108, 1);
  static const green = Color.fromRGBO(208, 235, 114, 1);
  static const blue = Color.fromRGBO(162, 226, 246, 1);
  static const yellowLight = Color.fromRGBO(245, 221, 144, 1);
}
