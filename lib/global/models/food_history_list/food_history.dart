class FoodHistoryData {
  final String name;
  final double gramms;
  final String date;
  final double ccal;

  FoodHistoryData({
    required this.name,
    required this.gramms,
    required this.date,
    required this.ccal,
  });
}
