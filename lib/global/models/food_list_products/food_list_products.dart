class FoodListProducts {
  final int id;
  final String foodName;
  late final String kcalProduct;
  final String fatsProduct;
  final String carbsProduct;
  final String proteinsProduct;

  FoodListProducts({
    required this.id,
    required this.foodName,
    required this.kcalProduct,
    required this.fatsProduct,
    required this.carbsProduct,
    required this.proteinsProduct,
  });
}
