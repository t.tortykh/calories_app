class WeekStatCardModel {
  final String date;
  final String month;
  final int carbs;
  final int proteins;
  final int fats;

  WeekStatCardModel({
    required this.date,
    required this.month,
    required this.carbs,
    required this.proteins,
    required this.fats,
  });
}
