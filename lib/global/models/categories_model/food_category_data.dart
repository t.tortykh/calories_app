class FoodCategoriesData {
  final int id;
  final String name;
  final String description;
  final String img;
  // final int sort;
  // final int active;
  // final String createdAt;
  // final String updatedAt;

  FoodCategoriesData({
    required this.id,
    required this.name,
    required this.description,
    required this.img,
    // required this.sort,
    // required this.active,
    // required this.createdAt,
    // required this.updatedAt,
  });

  factory FoodCategoriesData.fromJson(Map<String, dynamic> json) {
    return FoodCategoriesData(
      id: json['id'] as int,
      name: json['name'] as String,
      description: json['description'] as String,
      img: json['img'] as String,
      // sort: json['sort'] as int,
      // active: json['active'] as int,
      // createdAt: json['created_at'] as String,
      // updatedAt: json['updated_at'] as String,
    );
  }
  // @override
  // String toString() {
  //   return 'FoodCategoriesData {id: $id, name: $name, description: $description, img: $img}';
  // }
}


//  final int id;
//   final String name;
//   final String image;
//   final String description;

//   const FoodCategoriesCard({
//     super.key,
//     required this.id,
//     required this.name,
//     required this.image,
//     required this.description,
//   });