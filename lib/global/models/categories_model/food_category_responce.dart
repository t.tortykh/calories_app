import 'package:calories/global/models/categories_model/food_category_data.dart';

class FoodCategoryResponce {
  bool success;
  List<FoodCategoriesData> categories;

  FoodCategoryResponce({
    required this.success,
    required this.categories,
  });

  factory FoodCategoryResponce.fromJson(Map<String, dynamic> json) {
    return FoodCategoryResponce(
      success: json['success'] as bool,
      categories: (json['categories'] as List<dynamic>)
          .map(
            (dynamic e) =>
                FoodCategoriesData.fromJson(e as Map<String, dynamic>),
          )
          .toList(),
    );
  }
}
