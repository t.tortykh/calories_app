import 'dart:io';
import 'package:calories/global/widgets/errors/auth_error.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:calories/global/providers/session_data_provider/session_data_provider.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';

class HttpClientApi {
  var context;

  Map<String, dynamic>? errorResponse;
  String error = "";

  final _sessionTokenProvider = SessionDataProvider();
  sendRequest(
      {required String url,
      required String method,
      body = null,
      headers = null}) async {
    // try {
    final _url = Uri.parse(dotenv.env['API_URL']! + url);
    var token = await _sessionTokenProvider.getToken().then((value) => value);

    var _headers = headers ??
        {
          "X-Requested-With": "XMLHttpRequest",
          'Authorization': 'Bearer $token',
        };
    var response;

    if (method == "post") {
      response = await http.post(_url, headers: _headers, body: body);
    } else if (method == "delete") {
      response = await http.delete(_url, headers: _headers, body: body);
    } else if (method == "get") {
      response = await http.get(_url, headers: _headers);
    } else {
      throw Exception('Method not found');
    }

    var responseData = json.decode(response.body);
    // print(responseData);
    if (response.statusCode == 401) {
      throw AuthException();
    } else if (response.statusCode == 500) {
      throw Exception('Somthing went wrong...');
    } else if (response.statusCode == 422) {
      errorResponse = responseData;
      var errorMessage = '';
      if (responseData['error'] != null &&
          responseData['error'] == "validation_error") {
        responseData['errors'].forEach((item, val) {
          val.forEach((er) {
            errorMessage += er + ' ';
          });
        });
      }
      throw Exception(
        errorMessage != ''
            ? errorMessage
            : responseData['message'] ??
                responseData['error'] ??
                'Unexpected error',
      );
    } else if (response.statusCode == 200) {
      // {success: true, result: response}
      return responseData;
    } else {
      throw Exception('Faild connection');
    }
    // } on AuthException catch (e) {
    //   Navigator.of(context)
    //       .pushReplacementNamed(MainNavigationRouteNames.login);
    // } catch (e) {
    //   rootScaffoldMessengerKey.currentState
    //       ?.showSnackBar(SnackBar(content: Text(e.toString())));

    // }
  }

  mediaRequest(url, File image) async {
    var token = await _sessionTokenProvider.getToken().then((value) => value);
    var request =
        http.MultipartRequest('POST', Uri.parse(dotenv.env['API_URL']! + url));
    request.headers.addAll({
      "X-Requested-With": "XMLHttpRequest",
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer $token',
    });

    request.files.add(await http.MultipartFile.fromPath(
      'image',
      image.path,
      contentType: MediaType.parse('multipart/form-data'),
    ));

    var response = await request.send();
    response.stream.transform(utf8.decoder).listen((value) {});
  }
}
