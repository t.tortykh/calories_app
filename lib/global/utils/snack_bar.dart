import 'package:calories/global/themes/theme/app_colors.dart';
import 'package:calories/lib/global/themes/resources/resources.dart';
import 'package:flutter/material.dart';

SnackBar createSnackBar({String? showedText, bool? result}) {
  return SnackBar(
    behavior: SnackBarBehavior.floating,
    backgroundColor: Colors.transparent,
    elevation: 0,
    content: Container(
      padding: const EdgeInsets.all(2),
      height: 90,
      decoration: result ?? false
          ? BoxDecoration(
              border: Border.all(color: Colors.black, width: 3),
              color: AppColor.green,
              borderRadius: const BorderRadius.all(Radius.circular(20)))
          : BoxDecoration(
              border: Border.all(color: Colors.black, width: 3),
              color: const Color.fromARGB(255, 229, 112, 126),
              borderRadius: const BorderRadius.all(Radius.circular(20))),
      child: Row(children: [
        result ?? false
            ? Image.asset(AppImages.check)
            : Image.asset(AppImages.cross),
        const SizedBox(width: 10),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              result ?? false
                  ? const Text(
                      'Success!',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontFamily: 'Rajdhani',
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1.5,
                      ),
                    )
                  : const Text(
                      'Oops!',
                      style: TextStyle(
                        fontSize: 18,
                        fontFamily: 'Rajdhani',
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1.5,
                      ),
                    ),
              Text(
                showedText!,
                style: const TextStyle(
                  color: Colors.black,
                  fontFamily: 'Rajdhani',
                ),
                overflow: TextOverflow.ellipsis,
                maxLines: 3,
              ),
            ],
          ),
        ),
      ]),
    ),
  );
}
