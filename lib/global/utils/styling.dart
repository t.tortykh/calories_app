import 'package:calories/global/themes/theme/app_colors.dart';
import 'package:flutter/material.dart';

class CustomTextStyle {
  static const TextStyle userSettingsTextStyle = TextStyle(
    letterSpacing: 2,
    fontSize: 35,
    fontWeight: FontWeight.bold,
    color: AppColor.yellow,
    shadows: [
      Shadow(
          // bottomLeft
          offset: Offset(-1, -1),
          color: Colors.black),
      Shadow(
          // bottomRight
          offset: Offset(1, -1),
          color: Colors.black),
      Shadow(
          // topRight
          offset: Offset(1, 1),
          color: Colors.black),
      Shadow(
          // topLeft
          offset: Offset(-1, 1),
          color: Colors.black),
    ],
  );
}
