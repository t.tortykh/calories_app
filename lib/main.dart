import 'package:calories/global/navigator/main_navigation.dart';
import 'package:calories/global/themes/theme/app_colors.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'package:flutter/material.dart';

main() async {
  await dotenv.load(fileName: '.env');
  runApp(const MyApp());
}

final GlobalKey<ScaffoldMessengerState> rootScaffoldMessengerKey =
    GlobalKey<ScaffoldMessengerState>();

class MyApp extends StatelessWidget {
  static final mainNavigation = MainNavigation();

  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      scaffoldMessengerKey: rootScaffoldMessengerKey,
      debugShowCheckedModeBanner: false,
      title: 'Calories',
      theme: ThemeData(
        fontFamily: 'Rajdhani',
        appBarTheme: const AppBarTheme(
            elevation: 0,
            foregroundColor: AppColor.tealLight,
            color: Colors.transparent,
            titleTextStyle: TextStyle(
                fontSize: 35,
                color: AppColor.tealLight,
                fontFamily: 'Rajdhani',
                fontWeight: FontWeight.bold)),
      ),
      routes: mainNavigation.route,
      initialRoute: MainNavigationRouteNames.mainScreen,
      onGenerateRoute: ((RouteSettings settings) {
        return MaterialPageRoute<void>(builder: (context) {
          return Scaffold(
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  const Text(
                    'Page is not found :(',
                    style: TextStyle(fontSize: 35),
                  ),
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          side: const BorderSide(
                            width: 4,
                            color: Colors.black,
                          ),
                          backgroundColor: AppColor.coral),
                      onPressed: () {
                        Navigator.pushNamedAndRemoveUntil(
                            context,
                            MainNavigationRouteNames.mainScreen,
                            ModalRoute.withName(''));
                      },
                      child: const Text('Main Screen'))
                ],
              ),
            ),
          );
        });
      }),
    );
  }
}
